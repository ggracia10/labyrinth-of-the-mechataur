﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] spawner;
    public int numSpawners;
    float temporizator, tempMax;
    int[] positions = new int[4];

	// Use this for initialization
	void Start () {
        tempMax = 10;
        temporizator = 0;
        resetChest();
		for (int i = 0; i < 100; i++)
        {
            if (GameObject.Find ("Spawner"+i) == null)
            {
                numSpawners = i;
                break;
            }
        }
        spawner = new GameObject[numSpawners];
        for (int i = 0; i < numSpawners; i++)
        {
            spawner[i] = GameObject.Find("Spawner" + i);
        }
        for (int i = 1; i < 5; i++)
        {
            GameObject spawn = Instantiate(Resources.Load<GameObject>("Chest"));
            spawn.GetComponent<Transform>().position = spawner[i].transform.position;
            spawn.name = "Chest" + (i - 1);
        }
	}
	
	// Update is called once per frame
	void Update () {
        temporizator += Time.deltaTime;
        if (temporizator >= tempMax) {
            
            resetChest();
            for (int i = 0; i < 4; i++)
            {
                GameObject spawn = Instantiate(Resources.Load<GameObject>("Chest"));
                int randNum = 10;
                while (randNum == positions[0] || randNum == positions[1] ||
                    randNum == positions[2] || randNum == positions[3])
                {
                    randNum = Random.Range(0, numSpawners);
                }
                positions[i] = randNum;
                spawn.GetComponent<Transform>().position = spawner[randNum].transform.position;
                spawn.name = "Chest" + i;
            }
            temporizator = 0;
        }
	}
    void resetChest()
    {
        for (int i = 0; i < 4; i++)
        {
            Destroy(GameObject.Find("Chest" + i));
            positions[i] = 10;
        }
    }
}
