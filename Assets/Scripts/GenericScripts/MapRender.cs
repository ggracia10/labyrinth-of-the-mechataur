﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tiled2Unity;

public class MapRender : MonoBehaviour {

    public int numLayers = 0;
    public int numLayersBack = 0;
    public int numLayersFront = 0;
    public int numb;

	// Use this for initialization
	void Start () {
        numb = 0;
        numLayers = gameObject.transform.childCount;
        for (int i = 1; i < numLayers; i++) {
            Renderer obj = gameObject.transform.GetChild(i).GetComponentInChildren<Renderer>(); ;
            string name = gameObject.transform.GetChild(i).name;
            string newNumb = name.Substring(1, name.Length - 1);
            int layer = GenericFunctions.stoi(newNumb);
            if (name[0] == 'B')
            {
                obj.sortingOrder = 180 * (30 - layer - 1) + 90;

                //string newNumb = "" + name[1] + name[2];
            }
            else if (name[0] == 'F')
            {
                obj.sortingOrder = 180 * (30 - layer + 1) - 90;
            }
        }
    }
	
	// Update is called once per frame
	
}
