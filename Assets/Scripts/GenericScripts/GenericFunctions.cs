﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GenericFunctions
{
    public enum Scenes { MainMenu, CharacterSelection, Options, Game, Credits, EndGame};
    //! Set the render order of the sprites (determinated by the (negative) Y position of each sprite)
    public static void setRenderOrder(GameObject thisObject)
    {
        int tempOrder = -(int)Mathf.Round(thisObject.transform.position.y * 100);
        if (thisObject.name == "Skin" || thisObject.name == "Weapon")
        {
            tempOrder++;
        }
        if (thisObject.tag == "DropWeapon")
        {
            tempOrder -= 100;
        }

        thisObject.GetComponent<SpriteRenderer>().sortingOrder = tempOrder;
        
        return;
    }

    //! The size of the world it can't be changed by scripting
    static Vector2 worldSize = (GameObject.Find("upRightCornerMap").GetComponent<Transform>().position -
        GameObject.Find("downLeftCornerMap").GetComponent<Transform>().position);

    //! Get the size of the world;
    public static Vector2 getWorldSize()
    {
        return (worldSize);
    }

    public static float radToDeg(float radAng)
    {
        float degAng = (180 * radAng) / Mathf.PI;
        return degAng;
    }
    public static float degToRad(float degAng)
    {
        float radAng = (Mathf.PI * degAng) / 180;
        return radAng;
    }

    public static int stoi (string num)
    {
        int number = 0;
        int length = num.Length;
        int[] numbArray = new int[length];
        for (int i = 0; i < num.Length; i++)
        {
            numbArray[i] = num[i] - 48;
        }
        for (int i = 0; i < numbArray.Length; i++)
        {
            if (i < numbArray.Length - 1) number += (numbArray[i]) * 10 * (numbArray.Length - i - 1);
            else number += (numbArray[i]);

        }
        //Debug.Log("number is: " + num + " | " + number);
        return number;
    }

    //pastel tones
    public static Color pastelViolet = new Color(230F / 255F, 185F / 255F, 235F / 255F, 1);
    public static Color pastelBlue = new Color(150F / 255F, 255F / 255F, 255F / 255F, 1);
    public static Color pastelGreen = new Color(170F / 255F, 251F / 255F, 180F / 255F, 1);
    public static Color pastelPink = new Color(255F / 255F, 182F / 255F, 193F / 255F, 1);
    public static Color pastelYellow = new Color(255F / 255, 250F / 255F, 205F / 255F);

    //Neon tones
    public static Color neonRed = new Color(1, 0.25F, 0.25F, 1);
    public static Color neonGreen = new Color(0, 1, 0, 1);
    public static Color neonBlue = new Color(50F / 255F, 80F / 255F, 255F / 255F);
    public static Color neonTurquoise = new Color(0,1,1,1);

    //CMYK
    public static Color white = new Color(1, 1, 1, 1);
    public static Color cyan = new Color(0, 1, 1, 1);
    public static Color yellow = new Color(1, 1, 0, 1);
    public static Color magenta = new Color(1, 0, 1, 1);

    public static Color darkGreen = new Color(3F/ 255F, 150F / 255F, 13F / 255F, 1);
    public static Color darkRed = new Color(150F / 255F, 3F / 255F, 13F / 255F, 1);
    public static Color darkBlue = new Color(13F / 255F, 3F / 255F, 150F / 255F, 1);
    public static Color darkYellow = new Color(215F / 255F, 195F / 255F, 42F / 255F, 1);
    
}
   
