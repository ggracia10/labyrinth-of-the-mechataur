﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    Transform trans;
    Camera mainCamera;
    Vector3 cameraPosition;
    const int minSize = 6;
    const int maxSize = 27;
    float cameraSizeX, cameraSizeY, cameraSize;
    public float maxX, maxY, minX, minY;
    GameObject [] players = new GameObject[4];
    float tempIni, tempMaxIni;
    bool shaking;
    float timeShaking, timeMaxShaking;

	//! Use this for initialization
	void Start () {
        tempIni = 0;
        tempMaxIni = 3;
        for (int i = 0; i < players.Length; i++)
        {
            players[i]= GameObject.Find("Model" + i);
        }
        trans = gameObject.transform;
        mainCamera = gameObject.GetComponent<Camera>();
        cameraSize = maxSize;
        mainCamera.orthographicSize = cameraSize;
        timeShaking = 0;
        timeMaxShaking = 0.5F;
	}
	
	//! Update is called once per frame
	void Update () {
        if(tempIni < tempMaxIni && cameraSize > 8.6334F)
        {
            tempIni += Time.deltaTime;
            if (tempIni >= 1)
            {
                cameraSize -= 9 * Time.deltaTime;
            }
            mainCamera.orthographicSize = cameraSize;
            return;
        }
        calculateMaxPositions();
        resizeCamera();
        calculateCentralPoint();
        trans.position = cameraPosition;
        if(shaking)
        {
            ShakeCamera();
        }
	}

    //! Calculate the position of the camera (the center point between the players)
    void calculateCentralPoint()
    {
        Vector3 centralPoint = new Vector3(0, 0, 0);
        Vector2 lineBetweenPoints = new Vector2(maxX - minX, maxY - minY);
        centralPoint = new Vector2(lineBetweenPoints.x, lineBetweenPoints.y) / 2 + new Vector2 (minX, minY-0.5F);
        cameraPosition = new Vector3(centralPoint.x, centralPoint.y, -100);
        return;
    }

    //! Calculate the max positions of the players (max and min X, max and min Y)
    void calculateMaxPositions()
    {
        minX = 1000;
        maxX = -1000;
        maxY = -1000;
        minY = 1000;

        //! We check which player is on the Top
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].activeSelf)
            {
                if (players[i].transform.position.y > maxY)
                {
                    maxY = players[i].transform.position.y;
                }
            }
        }
        //! We check which player is on the Bot
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].activeSelf)
            {
                if (players[i].transform.position.y < minY)
                {
                    minY = players[i].transform.position.y;
                }
            }
        }
        //! We check which player is on the Left
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].activeSelf)
            {
                if (players[i].transform.position.x < minX)
                {
                    minX = players[i].transform.position.x;
                }
            }
        }
        //! We check which player is on the Right
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].activeSelf)
            {
                if (players[i].transform.position.x > maxX)
                {
                    maxX = players[i].transform.position.x;
                }
            }
        }
        return;
    }
    
    void resizeCamera()
    {
        //! Cross multiplication to know the relative size of the camera with the map
        cameraSizeY = minSize + (((maxSize - minSize) * (maxY - minY)) / GenericFunctions.getWorldSize().y);
        cameraSizeX = minSize + (((maxSize - minSize) * (maxX - minX)) / GenericFunctions.getWorldSize().x);
        //! Check the two sizes and choose the biggest one
        if (cameraSizeX > cameraSizeY)
        {
            cameraSize = cameraSizeX;
        }
        else
        {
            cameraSize = cameraSizeY;
        }
        //! Check the size of the camera not beeing bigger than the max
        if (cameraSize >= maxSize)
        {
            cameraSize = maxSize;
        }
        if (cameraSize <= minSize)
        {
            cameraSize = minSize;
        }
        //! The resize of the camera
        mainCamera.orthographicSize = cameraSize;
        return;
    }

    public void ShakeCamera()
    {
        timeShaking += Time.deltaTime;
        if(timeShaking < 0.2F)
        {
            return;
        }
        if (timeShaking>=timeMaxShaking)
        {
            shaking = false;
            timeShaking = 0;
            return;
        }
        Vector3 shakingVect = new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), 0);
        trans.position += shakingVect * 0.2F;
    }

    public void setShaking(bool newBool)
    {
        shaking = newBool;
    }

    public bool getShaking() { return shaking; }

}
