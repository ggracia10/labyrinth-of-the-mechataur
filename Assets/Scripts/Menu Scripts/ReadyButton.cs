﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyButton : MonoBehaviour {
    public string gameObjectName;
    protected int buttonActive;
	// Use this for initialization
	void Start ()
    {
        buttonActive = 0;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" && col.name == gameObjectName)
        {
            buttonActive = 1;
        }

    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player" && col.name == gameObjectName) { // detect also if the player have a crown (A child)
            buttonActive = 0;
        }
    }

    public int GetButtonActive() { return buttonActive; }
}
