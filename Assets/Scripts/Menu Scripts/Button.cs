﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Button : MonoBehaviour {

    private float activationTimer;
    private bool collisionTrigger;
    public bool CrownRequired;
    public bool isButtonExit;
    public bool isOptions;
    public bool isCredits;
    public bool isButtonMain;
    Color[] menuColor = new Color[4];
    public GameObject loading;

    [SerializeField]
    GenericFunctions.Scenes ButtonType;

    // Use this for initialization
    void Start () {
        loading = gameObject.transform.GetChild(0).gameObject;
        /*menuColor[0] = GenericFunctions.darkGreen;
        menuColor[1] = GenericFunctions.neonGreen;
        menuColor[2] = GenericFunctions.darkRed;
        menuColor[3] = GenericFunctions.neonRed;
        menuColor[4] = GenericFunctions.darkBlue;
        menuColor[5] = GenericFunctions.neonBlue;*/
        //menuColor[6] = 
        activationTimer = 3;
        if(gameObject.name == "ButtonExit")
        {
            isButtonExit = true;
        }
        if(gameObject.name == "ButtonMain")
        {
            isButtonMain = true;
        }
        else if(gameObject.name == "ButtonOptions")
        {
            isOptions = true;
        }
        else if (gameObject.name == "ButtonCredits")
        {
            isCredits = true;
        }
        loading.transform.localScale = new Vector3 (0,1.7F,1);
        if (isButtonExit || isButtonMain)
        {
            loading.GetComponent<SpriteRenderer>().color = GenericFunctions.neonRed;
        }
        else if(isOptions)
        {
            loading.GetComponent<SpriteRenderer>().color = GenericFunctions.neonBlue;
        }
        else if (isCredits)
        {
            loading.GetComponent<SpriteRenderer>().color = GenericFunctions.yellow;
        }
        else
        {
            loading.GetComponent<SpriteRenderer>().color = GenericFunctions.neonGreen;
        }

        loading.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (collisionTrigger)
        {
            Debug.Log(activationTimer);
            //loading.SetActive(true);
            loading.transform.localScale = new Vector3(1.75F * (3 - activationTimer) / 3, 1.7F, 1);
            activationTimer -= Time.deltaTime;
            //Debug.Log(isButtonMain);
            if(activationTimer <= 0)
            {
                activationTimer = 3;
                collisionTrigger = false;
                
                if (isButtonExit)
                {
                    Debug.Log("HOLII");
                    Application.Quit();
                }
                else if(isButtonMain)
                {
                    Debug.Log("HOLII");
                    GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.MainMenu);
                }
                else if(isOptions)
                {
                    GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.Options);
                }
                else if(isCredits)
                {
                    GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.Credits);
                }
                else
                {
                    GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.CharacterSelection);
                }
            }

        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" && col.transform.Find("Crown"))
        {
            collisionTrigger = true;
            loading.SetActive(true);
            GameObject.Find("GameState").GetComponent<GameState>().setPlayerKing(GenericFunctions.stoi("0"+col.gameObject.name[9]));
        }

    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (isButtonExit || isButtonMain)
        {
            gameObject.GetComponent<SpriteRenderer>().color = GenericFunctions.darkRed;
        }
        else if(isOptions)
        {
            gameObject.GetComponent<SpriteRenderer>().color = GenericFunctions.darkBlue;
        }
        else if (isCredits)
        {
            gameObject.GetComponent<SpriteRenderer>().color = GenericFunctions.darkYellow;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = GenericFunctions.darkGreen;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player" && col.transform.Find("Crown")) { // detect also if the player have a crown (A child)
            collisionTrigger = false;
            activationTimer = 3;
            loading.SetActive(false);
        }
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
    
}
