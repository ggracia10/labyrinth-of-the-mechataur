﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System.IO;
using UnityEngine.SceneManagement;

public class PlayerMenu : Entity
{
    int player;
    private Player playerInput;
    infoEntity info;
    bool hitBullet = false;
    public bool stunedBool = false;
    public bool pushed = false;
    public bool triggerBox = false;
    public Vector2 pushedVector = new Vector2(0, 0);
    PauseMenu pause;

    //time for traps
    private float delayTimeTrap;
    int counterTrap;

    //sounds
    public AudioClip shotSound;
    public AudioSource shotSoundSource;
    public AudioClip walkSound;
    public AudioSource walkSoundSource;

    //Particles
    public ParticleSystem deathParticle;
    private float deathTemp;
    private bool deathBool = false;

    //GameState
    private GameState infoGame;

    private void Awake()
    {
        filePath = "./Files/Stats/PlayerStats.json";
        jsonString = File.ReadAllText(filePath);
        info = JsonUtility.FromJson<infoEntity>(jsonString);
        myStats = new Stats(info.name, info.maxHP, info.attack, info.defense, info.maxSpeed,
            Weapon.WeaponEnum.None, 1, GenericFunctions.white);

        shotSound = Resources.Load("Sounds/uziShot") as AudioClip;
        shotSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        walkSound = Resources.Load("Sounds/walkSound") as AudioClip;
        walkSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        walkSoundSource.clip = walkSound;
        walkSoundSource.loop = true;
    }

    //!  Use this for initialization
    void Start()
    {
        infoGame = GameObject.Find("GameState").GetComponent<GameState>();
        //particles
        timeRocketImpact = 0;
        timeMaxRocketImpact = 5;
        deathParticle = gameObject.transform.GetChild(5).GetComponent<ParticleSystem>();
        deathParticle.Stop();
        deathTemp = 1;

        mSpriteRender = gameObject.GetComponent<SpriteRenderer>();
        currentSpeedX = 0;
        currentSpeedY = 0;
        currentAngle = 0;
        changeAnim = false;
        moving = false;
        //Debug.Log(myStats);
        currentDirection = Direction.Down;
        lastDirection = currentDirection;
        trans = gameObject.transform;
        anim = gameObject.GetComponent<Animator>();
        animSkin = gameObject.transform.GetChild(0).GetComponent<Animator>();
        animWeapon = gameObject.transform.GetChild(6).GetComponent<Animator>();
        attacking = false;
        timeStuned = 0;
        timeStunedMax = 1;
        switch (gameObject.name)
        {
            case "ModelMenu0":
                player = 0;
                break;
            case "ModelMenu1":
                player = 1;
                break;
            case "ModelMenu2":
                player = 2;
                break;
            case "ModelMenu3":
                player = 3;
                break;
        }
        ChangeColor(infoGame.getPlayerColors(player), player);
        myStats.name = "ModelMenu";
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        playerInput = ReInput.players.GetPlayer(player);
        tempInvencible = 0;
        tempInvencibleMax = 1;
    }

    //!  Update is called once per frame
    void Update()
    {
        hitBullet = false;
        if (currentCoolDown != coolDown)
        {
            currentCoolDown += Time.deltaTime;
            if (currentCoolDown >= coolDown)
            {
                currentCoolDown = coolDown;
            }
        }
        tempInvencible += Time.deltaTime;
        ResetVariables();
        if (!stunedBool)
        {
            CheckControls();
            Movement(currentSpeedX, currentSpeedY, currentAngle, moving);
        }
        else
        {
            timeStuned += Time.deltaTime;
            if (pushed)
            {
                tempInvencible = 0;
                float desaccel = 2F;
                if (pushedVector.x > 0)
                {
                    pushedVector -= new Vector2(desaccel, 0);
                }
                else if (pushedVector.x < 0)
                {
                    pushedVector -= new Vector2(-desaccel, 0);
                }
                else if (pushedVector.y > 0)
                {
                    pushedVector -= new Vector2(0, desaccel);
                }
                else if (pushedVector.y < 0)
                {
                    pushedVector -= new Vector2(0, -desaccel);
                }

                if (pushedVector == new Vector2(0, 0))
                {
                    pushed = false;
                    stunedBool = false;
                    timeStuned = 0;
                    pushedVector = new Vector2(0, 0);
                }
                Movement(pushedVector.x, pushedVector.y, 0, true);
            }
            if (timeStuned >= timeStunedMax)
            {
                stunedBool = false;
                pushed = false;
                timeStuned = 0;
                pushedVector = new Vector2(0, 0);
            }
        }

        ChangeAnimation(currentSpeedX, currentSpeedY, attacking, (int)myStats.currentWeapon, stuned, changeAnim,
            (int)currentDirection, myStats);
        lastDirection = currentDirection;
    }

    //! Check controls of the player.
    protected void CheckControls()
    { 
        if (currentCoolDown >= coolDown)
        {
            if (myStats.currentWeapon != Weapon.WeaponEnum.Uzi)
            {
                if (playerInput.GetButtonDown("Attack"))
                {
                    Attack(myStats);

                    currentCoolDown = 0;
                }
            }
            else
            {
                if (playerInput.GetButton("Attack"))
                {
                    Attack(myStats);

                    currentCoolDown = 0;
                }
            }

        }

        //the pick weapon input is in OnTriggerStay2D

        if (playerInput.GetAxis("MoveX") < -0.3F || playerInput.GetButton("MoveLeft"))
        {
            currentSpeedX = -myStats.maxSpeed;
            moving = true;
        }
        else if (playerInput.GetAxis("MoveX") > 0.3F || playerInput.GetButton("MoveRight"))
        {
            currentSpeedX = myStats.maxSpeed;
            moving = true;
        }
        if (playerInput.GetAxis("MoveY") < -0.3F || playerInput.GetButton("MoveDown"))
        {
            currentSpeedY = -myStats.maxSpeed;
            moving = true;
        }
        else if (playerInput.GetAxis("MoveY") > 0.3F || playerInput.GetButton("MoveUp"))
        {
            currentSpeedY = myStats.maxSpeed;
            moving = true;
        }

        if (currentSpeedX != 0 && currentSpeedY != 0)
        {
            currentAngle = Mathf.PI / 4;
        }

        /*currentSpeedX = myStats.maxSpeed * playerInput.GetAxis("MoveX");
        currentSpeedY = myStats.maxSpeed * playerInput.GetAxis("MoveY");*/
        if (currentSpeedX != 0 || currentSpeedY != 0)
        {
            moving = true;
            if (!walkSoundSource.isPlaying)
            {
                walkSoundSource.Play();
            }
        }
        else
        {
            walkSoundSource.Stop();
        }
        return;
    }

    //! This function set Position and Direction to an instance of a bullet
    void setDirectionBullet(Vector2 newVect, Vector3 newPos, GameObject newAttack, Stats newStats)
    {
        float offsetS = newAttack.GetComponent<Collider2D>().bounds.size.x / 2F;
        switch (currentDirection)
        {
            case Direction.Down:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(2).transform.position - new Vector3(0, 0.7F, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Down;
                if (myStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None)
                {
                    newAttack.GetComponent<BoxCollider2D>().offset = new Vector2(0.4f, 0F);
                    newAttack.GetComponent<BoxCollider2D>().size = new Vector2(0.9f, 1.25F);
                }
                break;
            case Direction.Up:

                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(1).transform.position + new Vector3(0, 2F, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Up;
                if (myStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None)
                {
                    newAttack.GetComponent<BoxCollider2D>().size = new Vector2(1.1f, 1.2F);
                    newAttack.GetComponent<BoxCollider2D>().offset = new Vector2(0.21f, 0F);
                }
                break;
            case Direction.Left:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(3).transform.position - new Vector3(0.5F, 0, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Left;
                break;
            case Direction.Right:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(4).transform.position + new Vector3(1F, 0, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Right;
                break;
        }
        if (!((newStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None) &&
            currentDirection == Direction.Up))
        {
            newAttack.GetComponent<Collider2D>().offset = newVect;
        }

        if (newStats.currentWeapon == Weapon.WeaponEnum.None ||
            newStats.currentWeapon == Weapon.WeaponEnum.Sword)
        {
            newAttack.GetComponent<Transform>().SetParent(gameObject.transform);
        }
        newAttack.GetComponent<Transform>().position = newPos;
    }

    override protected void Attack(Stats tempStats)
    {
        if (myStats.ammoCount <= 0)
        {
            return;
        }
        //Debug.Log(tempStats.name + ": " + tempStats.currentWeapon);
        string pathAttack = "Attack/AttackNone";
        Vector2 tempVect = new Vector2(0, 0);
        Vector3 tempPosition = new Vector3(0, 0, 0);
        myStats.ammoCount -= 1;
        shotSound = Resources.Load("Sounds/pushShot") as AudioClip;
        shotSoundSource.PlayOneShot(shotSound, 0.7F);

        GameObject attack = Instantiate(Resources.Load<GameObject>(pathAttack));

        setDirectionBullet(tempVect, tempPosition, attack, tempStats);
        //! code
        return;
    }

    override public void Stunned(Bullet.BulletEnum typeBullet, Direction newDir, float newDamage)
    {
        stunedBool = true;
        PushedFunction(newDir, typeBullet);
        if (gameObject.transform.Find("Crown"))
        {
            Debug.Log("crown drop");
            gameObject.transform.Find("Crown").transform.parent = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            //Debug.Log("TAG: " + collision.gameObject.tag);
            Bullet bulletCol = collision.gameObject.GetComponent<Bullet>();
            Debug.Log("Bool = " + hitBullet);
            if (!hitBullet || bulletCol.thisBullet.typeOfBullet == Bullet.BulletEnum.Shotgun)
            {
                if (bulletCol.thisBullet.typeOfBullet == Bullet.BulletEnum.RocketLauncher)
                {
                    if (timeRocketImpact < timeMaxRocketImpact) return;
                    timeRocketImpact = 0;
                }
                hitBullet = true;
                Stunned(bulletCol.thisBullet.typeOfBullet, bulletCol.thisBullet.direction, bulletCol.thisBullet.damage);
                bulletCol.GetComponent<Bullet>().Explosion();
            }
            Debug.Log("Bool2 = " + hitBullet);
            //Debug.Log("Type: " + bulletCol.thisBullet.typeOfBullet);

        }
    }

        private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Crown")
        {
            if (playerInput.GetButtonDown("Pick") && collision.transform.parent == null)
            {
                collision.transform.parent = gameObject.transform;
                collision.transform.localPosition = new Vector3(0, 2.47F, 0);
            }
        }
        if (collision.tag == "TriggerColor")
        {
            if (playerInput.GetButtonDown("Pick"))
            {
                ChangeColor(collision.GetComponent<SpriteRenderer>().color, player);
            }
        }
    }

    protected void PushedFunction(Direction newDir, Bullet.BulletEnum typeBullet)
    {
        pushed = true;

        int pushedSpeed = 0;
        if (typeBullet == Bullet.BulletEnum.None ||
            typeBullet == Bullet.BulletEnum.Sword)
        {
            pushedSpeed = 40;
        }
        else if (typeBullet == Bullet.BulletEnum.Shotgun)
        {
            pushedSpeed = 30;
        }
        switch (newDir)
        {
            case Direction.Left:
                pushedVector = new Vector2(-pushedSpeed, 0);
                break;
            case Direction.Right:
                pushedVector = new Vector2(pushedSpeed, 0);
                break;
            case Direction.Down:
                pushedVector = new Vector2(0, -pushedSpeed);
                break;
            case Direction.Up:
                pushedVector = new Vector2(0, pushedSpeed);
                break;
        }
    }
    public void ChangeColor(Color newColor, int player)
    {
        gameObject.GetComponent<SpriteRenderer>().color = newColor;
        infoGame.setColors(newColor, player);
        if(GameObject.Find("ButtonReadyArray")!= null)
        {
            GameObject.Find("ButtonReadyArray").GetComponent<ArrayButtonReady>().ChangeColor(newColor, player);
        }
        return;
    }





}



