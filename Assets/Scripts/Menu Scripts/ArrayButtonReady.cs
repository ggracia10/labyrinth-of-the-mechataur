﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArrayButtonReady : MonoBehaviour {
    private int readyCount;
    private float changeTimer;
    GameObject[] buttons = new GameObject[4];
    Image circle;

    private void Awake()
    {
        for (int i = 0; i < 4; i++)
        {
            buttons[i] = GameObject.Find("ReadyButton" + i);
            buttons[i].GetComponent<SpriteRenderer>().color = GameObject.Find("ModelMenu" + i).GetComponent<SpriteRenderer>().color;
        }
    }
    // Use this for initialization
    void Start () {
       
        readyCount = 0;
        changeTimer = 3;
        circle = GameObject.Find("Circle").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        circle.fillAmount = (3F - changeTimer) / 3F;
        readyCount = 0;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            readyCount += gameObject.transform.GetChild(i).GetComponent<ReadyButton>().GetButtonActive();
            
        }

        if(readyCount == 4){
            
            changeTimer -= Time.deltaTime;
            if (changeTimer <= 0)
            {
                GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.Game);
            }
        }
        else
        {
            changeTimer = 3;
        }
	}
    public void ChangeColor(Color newColor, int player) 
    {
        buttons[player].GetComponent<SpriteRenderer>().color = newColor;
        return;
    }
}
