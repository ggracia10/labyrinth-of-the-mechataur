﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerColor : MonoBehaviour {


    private enum Colors { PASTELVIOLET, PASTELBLUE, PASTELGREEN, PASTELPINK, PASTELYELLOW,
                          NEONRED, NEONGREEN, NEONBLUE, NEONTURQUOISE,
                          WHITE, CYAN, YELLOW, MAGENTA};

    [SerializeField]
    Colors ColorTile;

    Color colorbackground;

    // Use this for initialization
    void Start () {
        switch (ColorTile)
        {
            case Colors.PASTELVIOLET:
                colorbackground = GenericFunctions.pastelViolet;

                break;
            case Colors.PASTELBLUE:
                colorbackground = GenericFunctions.pastelBlue;
                break;
            case Colors.PASTELGREEN:
                colorbackground = GenericFunctions.pastelGreen;
                break;
            case Colors.PASTELPINK:
                colorbackground = GenericFunctions.pastelPink;
                break;
            case Colors.PASTELYELLOW:
                colorbackground = GenericFunctions.pastelYellow;
                break;
            case Colors.NEONRED:
                colorbackground = GenericFunctions.neonRed;
                break;
            case Colors.NEONGREEN:
                colorbackground = GenericFunctions.neonGreen;
                break;
            case Colors.NEONBLUE:
                colorbackground = GenericFunctions.neonBlue;
                break;
            case Colors.NEONTURQUOISE:
                
                colorbackground = GenericFunctions.neonTurquoise;
                break;
            case Colors.WHITE: default:
                colorbackground = GenericFunctions.white;
                break;
            case Colors.CYAN:
                colorbackground = GenericFunctions.cyan;
                break;
            case Colors.YELLOW:
                colorbackground = GenericFunctions.yellow;
                break;
            case Colors.MAGENTA:
                colorbackground = GenericFunctions.magenta;
                break;
        }
        gameObject.transform.GetComponent<SpriteRenderer>().color = colorbackground;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
