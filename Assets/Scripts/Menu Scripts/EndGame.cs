﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour {

    bool endGame;
    public Text[] scores = new Text[4];

	// Use this for initialization
	void Start () {
        for(int i = 0; i < 4; i++)
        {
            scores[i] = gameObject.transform.GetChild(i+5).GetComponent<Text>();
            //Debug.Log("HOLI");
            //scores[i] = GameObject.Find("Score" + i);
        }
        GameObject.Find("GameState").GetComponent<GameState>().end = gameObject;
        setEndGame(false);
	}

    // Update is called once per frame
    public void setEndGame(bool newBool)
    {
        endGame = newBool;
        gameObject.SetActive(newBool);
    }

    public bool getEndGame()
    {
        return endGame;
    }
    public void setScore(int[] array)
    {
        for (int i = 0; i < 4; i++)
        {
            scores[i].text = array[i].ToString();
        }
    }
}
