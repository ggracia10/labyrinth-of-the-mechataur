﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : MonoBehaviour {
    GameState game;
    Color playerColor;
    float time, timeMax;
    Text text;
	// Use this for initialization
	void Start () {
        timeMax = 3;
        time = 0;
        text = GameObject.Find("WinnerText").GetComponent<Text>();
        game = GameObject.Find("GameState").GetComponent<GameState>();
        playerColor = game.getPlayerColors(game.getWinner());
        text.color = playerColor;
        GameObject.Find("ModelMenu0").GetComponent<SpriteRenderer>().color = playerColor;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if(time >= timeMax)
        {
            time = 0;
            GameObject.Find("GameState").GetComponent<GameState>().ChangeScene((int)GenericFunctions.Scenes.CharacterSelection);
        }
	}
}
