﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text;
using Rewired;

public class ScriptOptions : MonoBehaviour {

    enum Languages { Castellano, English, Japanese };
    Languages lang;
    public string filePath, jsonString;
    public Text[] panel = new Text[8];
    public string[] text = new string[6];
    public Transform[] positions = new Transform[6];
    Resolution[] resolutions;
    public string[] qualityNames;
    public string[] resString;
    public int[] resWidth;
    public int[] resHeight;
    public int resMult;
    public int resolutionsNumber, pos, posLang, posGraph, posRes, volume;
    public bool options;
    public GameObject optionMenu;
    public Transform trans;
    GameState info;
    public Image volumeBar;
    ScriptSetFont font;
    int player;
    private Player playerInput;
    bool axisDown;
    float temp, tempMax;

    public void ResizeArray(int Size, ref string[] Group)
    {
        string[] temp = new string[Size];
        for (int c = 1; c < Mathf.Min(Size, Group.Length); c++)
        {
            temp[c] = Group[c];
        }
        Group = temp;
    }
    
    void setPanels()
    {
        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] = GameObject.Find("posOptions" + i).GetComponent<Transform>();
            panel[i] = GameObject.Find("TextOptions" + i).GetComponent<Text>();
        }
        panel[6] = GameObject.Find("TextLang").GetComponent<Text>();
        panel[7] = GameObject.Find("TextGraph").GetComponent<Text>();
        panel[8] = GameObject.Find("TextRes").GetComponent<Text>();
    }

    void setResolutionList()
    {
        resolutionsNumber = 1;
        for (int i = 1; i < resolutions.Length; i++)
        {
            if (resolutions[i].width != resolutions[i - 1].width || resolutions[i].height != resolutions[i - 1].height)
            {
                resolutionsNumber++;
            }
        }
        resMult = resolutions.Length / resolutionsNumber;
        resWidth = new int[resolutionsNumber];
        resHeight = new int[resolutionsNumber];
        ResizeArray(resolutionsNumber, ref resString);
        for (int i = 0; i < resolutionsNumber; i++)
        {
            resWidth[i] = resolutions[i * resMult].width;
            resHeight[i] = resolutions[i * resMult].height;
            resString[i] = resolutions[i * resMult].width + "x" + resolutions[i * resMult].height;
        }
    }

    // Use this for initialization

    void Start () {
        temp = 0;
        tempMax = 0.25F;
        axisDown = false;
        player = 0;
        info = GameObject.Find("GameState").GetComponent<GameState>();
        trans = GameObject.Find("SelectorOptions").GetComponent<Transform>();
        options = false;
        optionMenu = GameObject.Find("OptionsMenu");
        resolutions = Screen.resolutions;
        resolutionsNumber = 0;
        setPanels();
        setResolutionList();
        qualityNames = QualitySettings.names;
        posLang = info.lang;
        for (int i = 0; i < resolutionsNumber; i++)
        {
            if (Screen.currentResolution.width == resolutions[i*resMult].width && 
                Screen.currentResolution.height == resolutions[i * resMult].height)
            {
                info.res = i;
            }
        }
        posRes = info.res;
        font = GameObject.Find("SetFont").GetComponent<ScriptSetFont>();
        font.changeFont(panel[7]);
        font.changeFont(panel[8]);
        panel[8].text = resString[posRes];
        info.graph = QualitySettings.GetQualityLevel();
        posGraph = info.graph;
        panel[7].text = qualityNames[posGraph];
        changeLanguage();
        volumeBar = GameObject.Find("VolumeBar").GetComponent<Image>();
        AudioListener.volume = (float)info.volume / 10.0F;
        volumeBar.fillAmount = (float)info.volume / 10.0F;
        //optionMenu.SetActive(false);
        options = true;
        player = info.getPlayerKing();
        playerInput = ReInput.players.GetPlayer(player);
        //playerInput.controllers.maps.LoadMap(ControllerType.Joystick, player, 1, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (!options)
        {
            return;
        }

        if (playerInput.GetAxis("MoveY") < -0.3F && !axisDown || playerInput.GetButtonDown("MoveDown"))
        {
            pos++;
            if (pos > positions.Length - 1)
            {
                pos = 0;
            }
        }
        else if (playerInput.GetAxis("MoveY") > 0.3F && !axisDown || playerInput.GetButtonDown("MoveUp"))
        {
            pos--;
            if (pos < 0)
            {
                pos = positions.Length - 1;
            }
        }
        if (playerInput.GetAxis("MoveX") < -0.3F && !axisDown|| playerInput.GetButtonDown("MoveLeft"))
        {
            if (pos == 0 && info.volume > 0)
            {
                setVolume(-1);
            }
            else if (pos == 1)
            {
                setResolution(-1);
            }
            else if (pos == 2)
            {
                setGraphics(-1);
            }
            else if (pos == 3)
            {
                setLanguage(-1);
            }
        }
        else if (playerInput.GetAxis("MoveX") > 0.3F && !axisDown || playerInput.GetButtonDown("MoveRight"))
        {
            if (pos == 0 && info.volume < 10)
            {
                setVolume(1);
            }
            else if (pos == 1)
            {
                setResolution(1);
            }
            else if (pos == 2)
            {
                setGraphics(1);
            }
            else if (pos == 3)
            {
                setLanguage(1);

            }
        }
        

        if (playerInput.GetButtonDown("Pick") && pos == 5 ||
            playerInput.GetButtonDown("Attack"))
        {
            options = false;
            playerInput.controllers.maps.LoadMap(ControllerType.Joystick, player, 0, 0);
            info.ChangeScene((int)GenericFunctions.Scenes.MainMenu);
        }

        if (playerInput.GetAxis("MoveX") >= -0.1F && playerInput.GetAxis("MoveX") <= 0.1F &&
            playerInput.GetAxis("MoveY") >= -0.1F && playerInput.GetAxis("MoveY") <= 0.1F)
        {
            Debug.Log(playerInput.GetAxis("MoveX") + ", " + playerInput.GetAxis("MoveY"));
            axisDown = false;
            temp = 0;
        }
        else if(playerInput.GetAxis("MoveX") >= 0.3F || playerInput.GetAxis("MoveX") <= -0.3F ||
            playerInput.GetAxis("MoveY") >= 0.3F || playerInput.GetAxis("MoveY") <= -0.3F)
        {
            axisDown = true;
            temp += Time.deltaTime;
            if (temp >= tempMax)
            {
                axisDown = false;
                temp = 0;
            }
        }

        trans.position = positions[pos].position;
    }

    void setVolume(int increment)
    {
        info.volume += increment;
        AudioListener.volume = (float)info.volume / 10.0F;
        volumeBar.fillAmount = (float)info.volume / 10.0F;
    }

    void setResolution(int increment)
    {
        posRes += increment;
        if (posRes > resolutionsNumber - 1)
        {
            posRes = 0;
        }
        else if (posRes < 0)
        {
            posRes = resolutionsNumber - 1;
        }
        info.res = posRes;
        font.changeFont(panel[8]);
        panel[8].text = resString[posRes];
        Screen.SetResolution(resolutions[posRes * resMult].width, resolutions[posRes * resMult].height, true);
    }

    void setGraphics(int increment)
    {
        posGraph += increment;
        if (posGraph < 0)
        {
            posGraph = qualityNames.Length - 1;
        }
        else if (posGraph > qualityNames.Length - 1)
        {
            posGraph = 0;
        }
        font.changeFont(panel[7]);
        panel[7].text = qualityNames[posGraph];
        info.graph = posGraph;
        QualitySettings.SetQualityLevel(posGraph, true);
    }

    void setLanguage (int increment)
    {
        posLang += increment;
        if (posLang > 2)
        {
            posLang = 0;
        }
        else if (posLang < 0)
        {
            posLang = 2;
        }
        info.lang = posLang;
        changeLanguage();
    }
    public class menu
    {
        public string[] optionsMenu;
    }

    void loadTextMainMenu(Languages lang)
    {
        if (lang == Languages.Castellano)
        {
            filePath = "./Data/Texts/castellano.json";
        }
        else if (lang == Languages.English)
        {
            filePath = "./Data/Texts/english.json";
        }
        else if (lang == Languages.Japanese)
        {
            filePath = "./Data/Texts/japanese.json";
        }
        jsonString = File.ReadAllText(filePath);
        menu main = JsonUtility.FromJson<menu>(jsonString);
        for (int i = 0; i < 7; i++)
        {
            text[i] = main.optionsMenu[i];
            panel[i].text = text[i];
            font.changeFont(panel[i]);
        }
    }

    public void changeLanguage()
    {
        if (info.lang == 0)
        {
            loadTextMainMenu(Languages.Castellano);
        }
        else if (info.lang == 1)
        {
            loadTextMainMenu(Languages.English);
        }
        else if (info.lang == 2)
        {
            loadTextMainMenu(Languages.Japanese);
        }
    }
}
