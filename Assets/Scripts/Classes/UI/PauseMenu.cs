﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    bool pause;

	// Use this for initialization
	void Start () {
        setPause(false);
	}

    public void setPause(bool newPause)
    {
        pause = newPause;
        if (pause)
        {
            Time.timeScale = 0;
            gameObject.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);
        }
    }

    public bool getPause ()
    {
        return pause;
    }
}
