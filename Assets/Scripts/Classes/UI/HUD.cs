﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public Image healthBar, playerIcon, playerSkin, weaponIcon;
    public SpriteRenderer sprite;
    //Image weaponIcon;
    //! Current Health is the health displayed
    //! health is the health that actually has the player
    //! amountHealthPerFrame is the amount of health will decrease per frame
    float maxHealth, currentHealth, health, amountHealthPerFrame;
    bool up, down;

	// Use this for initialization
	void Awake () {
        //! This is to get the component Image of the X child (in order top to down begginig in 0)
        //! of this object (the object that has this script)
        healthBar = gameObject.transform.GetChild(0).GetComponent<Image>();
        playerIcon = gameObject.transform.GetChild(1).GetComponent<Image>();
        weaponIcon = gameObject.transform.GetChild(2).GetComponent<Image>();
        playerSkin = gameObject.transform.GetChild(3).GetComponent<Image>();
        //weaponIcon = gameObject.transform.GetChild(3).GetComponent<Image>();
        maxHealth = 1;
        currentHealth = maxHealth;
        health = maxHealth;
        amountHealthPerFrame = 120;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentHealth == health)
        {
            return;
        }
        //! If the health has decreased
        if (down)
        {
            currentHealth -= amountHealthPerFrame * Time.deltaTime;
            if (currentHealth <= health)
            {
                currentHealth = health;
            }
        }
        //! If the health has increased
        else if (up)
        {
            currentHealth += amountHealthPerFrame * Time.deltaTime;
            if (currentHealth >= health)
            {
                currentHealth = health;
            }
        }
        //! Set the health bar lenght (to display on the health bar quantity of health you have)
        healthBar.fillAmount = currentHealth / maxHealth;
    }

    //! To set health via other script
    public void SetHealth(float newHealth)
    {
        health = newHealth;
        if (health > currentHealth)
        {
            up = true;
            down = false;
        }
        else if (health < currentHealth)
        {
            up = false;
            down = true;
        }
        return;
    }

    //! To change the weapon on the HUD
    /*public void SetWeapon(Weapon newWeapon)
    {
        weaponIcon.sprite = Resources.Load<Sprite>("UI/Weapon/" + newWeapon + "HUD");
        return;
    }*/
     //! To set the initial valors like the max health, the color of the skin and the skin of the icon
    public void Init(Color newColor, int newMaxHealth, int newSkin)
    {
        playerIcon.color = newColor;
        maxHealth = newMaxHealth;
        health = maxHealth;
        currentHealth = maxHealth;
        Debug.Log(newSkin);
        playerSkin.sprite = Resources.Load<Sprite>("UI/Skins/ModelSkin"+newSkin+"HUD");
        healthBar.fillAmount = currentHealth / maxHealth;
        weaponIcon.sprite = Resources.Load<Sprite>("UI/Skins/WeaponHUD/None");
        //change sprite of the skin
        return;
    }

    public void ChangeWeapon(Weapon.WeaponEnum newWeapon)
    {
        //Here the enumerator is a string, so we can put the enumerator "a pincho" and it will be the string
        weaponIcon.sprite = Resources.Load<Sprite>("UI/WeaponHUD/" + newWeapon);
        
    }
}
