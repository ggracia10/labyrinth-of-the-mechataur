﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour {
    float count, maxCount;
    Text text;
	// Use this for initialization
	void Start () {
        maxCount = 3;
        count = maxCount;
        text = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        count -= Time.deltaTime;
        text.text = ((int)count + 1).ToString();
        if (count <= 0)
        {
            text.text = "GO!";
        }
        
        if (count <= -1)
        {
            gameObject.SetActive(false);
        }
	}
}
