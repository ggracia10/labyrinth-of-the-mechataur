﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour { 
    public float maxdelayTime, counterActivation, maxCounterActivation;
    public int damageperdelay;
    public bool cycleTrap, damageActive;

    public Color colorIddle, colorIddleSpikes, colorPreparation, colorDamage;

    public SpriteRenderer trapRender;
    Animator anim;

    //particle manager for the traps
    public ParticleSystem ElectricParticles;

    //the type of trap.
    public enum TrapsEnum{SPIKES, PEM};
    public TrapsEnum trapType;

    public enum TrapStatus { Desactive, Loading, Active};
    TrapStatus statusTrap;

    //sounds
    public AudioClip spikeSound;
    public AudioSource spikeSoundSource;

    // Use this for initialization
    void Start () {

        trapRender = gameObject.GetComponent<SpriteRenderer>();
        colorIddle = new Color(30F / 255F, 255F / 255F, 30F / 255F, 0.2F);
        colorIddleSpikes = new Color(30F / 255F, 30F / 255F, 255F / 255F, 0.2F);
        colorPreparation = new Color(255F / 255F, 255F / 255F, 30F / 255F, 0.2F);
        colorDamage = new Color(255F / 255F, 30F / 255F, 30F / 255F, 0.2F);
        //trapRender.color = colorIddle;
        trapType = (TrapsEnum)Random.Range(0, System.Enum.GetValues(typeof(TrapsEnum)).Length);
        cycleTrap = false;
        damageActive = false;
        anim = gameObject.GetComponent<Animator>();
        //trapType = TrapsEnum.SPIKES;

        //Particle instatiation
        ElectricParticles = gameObject.transform.GetChild(0).GetComponent<ParticleSystem>();
        ElectricParticles.Stop();

        spikeSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        spikeSoundSource.volume = 0.3F; ;

        switch (trapType)
        {
            case TrapsEnum.SPIKES:
                maxCounterActivation = 0.5F;
                //trapRender.color = colorIddleSpikes;
                maxdelayTime = 1.5F;
                damageperdelay = 30; //provisional
                gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case TrapsEnum.PEM:
                maxCounterActivation = 10;
                cycleTrap = true;
                maxdelayTime = 0.3f;
                damageperdelay = 8; //provisional
                break;
        }
        anim.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Traps/" + trapType + "Animator");
        GenericFunctions.setRenderOrder(gameObject);
    }

    private void Update()
    {
        switch (trapType)
        {
            case TrapsEnum.SPIKES:
                if (statusTrap!= TrapStatus.Desactive)
                {
                    counterActivation += Time.deltaTime;
                    switch (statusTrap)
                    {
                        case TrapStatus.Loading:
                            if (spikeSound != Resources.Load("Sounds/spikeLoading") as AudioClip)
                            {
                                spikeSound = Resources.Load("Sounds/spikeLoading") as AudioClip;
                                spikeSoundSource.PlayOneShot(spikeSound);
                            }
                            if (counterActivation >= maxCounterActivation)
                            {
                                changeStatus(TrapStatus.Active);

                                if (spikeSoundSource.isPlaying)
                                {
                                    spikeSoundSource.Stop();
                                }
                                spikeSound = Resources.Load("Sounds/spikeActive") as AudioClip;
                                spikeSoundSource.PlayOneShot(spikeSound);
                                counterActivation = 0;
                            }
                            
                            break;
                        case TrapStatus.Active:
                            if (counterActivation >= 3.5F*maxCounterActivation)
                            {
                                changeStatus(TrapStatus.Desactive);
                                counterActivation = 0;
                            }
                            break;
                    }
                }
                break;

            case TrapsEnum.PEM:
                counterActivation += Time.deltaTime;

                //! Esto es para que la PEM sea intermitente
                if (counterActivation >= maxCounterActivation)
                {
                    counterActivation = 0;
                    if (!damageActive)
                    {
                        if (!ElectricParticles.isPlaying)
                        {
                            ElectricParticles.Play();
                        }
                        changeStatus(TrapStatus.Active);
                    }
                    else
                    {
                        ElectricParticles.Stop();
                        changeStatus(TrapStatus.Desactive);
                    }
                }
                else if (counterActivation >= (0.9F * maxCounterActivation))
                {
                    changeStatus(TrapStatus.Loading);
                }
                
                break;
        }
    }

    public void changeStatus(TrapStatus newStatus)
    {
        switch (newStatus)
        {
            case TrapStatus.Loading:
                if (statusTrap != TrapStatus.Loading)
                {
                    statusTrap = TrapStatus.Loading;
                    //trapRender.color = colorPreparation;
                }
                break;
            case TrapStatus.Active:
                if (statusTrap != TrapStatus.Active)
                {
                    damageActive = true;
                    statusTrap = TrapStatus.Active;
                    //trapRender.color = colorDamage;
                }
                break;
            case TrapStatus.Desactive:
                if (statusTrap != TrapStatus.Desactive)
                {
                    damageActive = false;
                    //trapRender.color = colorIddle;
                    if (trapType == TrapsEnum.SPIKES)
                    {
                       // trapRender.color = colorIddleSpikes;
                    }
                    statusTrap = TrapStatus.Desactive;
                }
                break;
        }
        anim.SetInteger("statusTrap", (int)statusTrap);
    }
}
