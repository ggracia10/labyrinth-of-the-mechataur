﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameState : MonoBehaviour {

    static GameState info;
    int currentRound;
    int maxRound = 7;
    public int[] score = new int[4];
    GenericFunctions.Scenes currentScene;
    public int playersAlive = 0;
    public float tempMaxEndGame, tempEndGame;

    bool[] alivePlayer = new bool[4];
    public EventSystem eventSystem;
    private GameObject selectedButton;
    //PlayerClass player
    private bool buttonIsSelected;
    public Color[] playerColors = new Color[4];
    public GameObject end;
    public bool endGame;
    public bool introCamera;
    float tempIni, tempMaxIni;
    public int lang, volume, graph, res, playerKing, playerWinner;
    int[] lastScores = { 0, 0, 0, 0 };

    //Metele aqui todas las variables necesarias 

    private void Awake()
    {
        volume = 7;
        tempEndGame = 0;
        tempMaxEndGame = 3;
        end = GameObject.Find("EndGame");
        playerColors[0] = GenericFunctions.pastelPink;
        playerColors[1] = GenericFunctions.magenta;
        playerColors[2] = GenericFunctions.cyan;
        playerColors[3] = GenericFunctions.pastelYellow;
        if (info == null)
        {
            info = this;
            DontDestroyOnLoad(gameObject);
            
        }
        else if (info != this)
        {
            Destroy(gameObject);
        }
        
    }

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        tempIni = 0;
        tempMaxIni = 3;
        introCamera = true;
        playersAlive = 4;
        currentScene = GenericFunctions.Scenes.Game;
    }

    private void Update()
    {
        if (currentScene == GenericFunctions.Scenes.Game)
        {
            if (tempIni < tempMaxIni)
            {
                tempIni += Time.deltaTime;
                if (tempIni >= tempMaxIni)
                {
                    introCamera = false;
                }
            }
            if (endGame)
            {
                tempEndGame += 1.0F / 60.0F;
                if(tempEndGame < 0.2F)
                {
                    int tempAlive = 0;
                    for(int i = 0; i < 4; i++)
                    {
                        if (GameObject.Find("Model" + i) != null)
                            tempAlive++;
                    }
                    if (tempAlive == 0) score = lastScores;
                }
                if (tempEndGame >= tempMaxEndGame)
                {
                    tempEndGame = 0;
                    endGame = false;
                    ChangeScene((int)GenericFunctions.Scenes.Game);
                }
            }
        }
    }


    private void onDisable()
    {
        if (currentScene == GenericFunctions.Scenes.MainMenu)
        {
            buttonIsSelected = false;
        }
    }

    public void ChangeScene(int newScene)
    {
        Debug.Log("Current: " + currentScene);
        Debug.Log("New: " + (GenericFunctions.Scenes)newScene);
        if (newScene == (int)GenericFunctions.Scenes.Game)
        {
            if ((int)currentScene != newScene)
            {
                ResetScore();
            }
            playersAlive = 4;
            for (int i = 0; i < 4; i++)
            {
                alivePlayer[i] = true;
            }
        }
        tempIni = 0;
        tempMaxIni = 3;
        introCamera = true;
        currentScene = (GenericFunctions.Scenes) newScene;
        SceneManager.LoadScene(newScene);
    }
    public void LeaveGame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
    public void setColors(Color newColor, int newPlayer)
    {
        playerColors[newPlayer] = newColor;
        return;
    }
    public Color getPlayerColors(int newPlayer)
    {
        Debug.Log(newPlayer);
        return playerColors[newPlayer];
    }

    public int getWinner()
    {
        playerWinner = -1;
        int maxScore = -5;
        int counter = 0;
        for (int i = 0; i < 4; i++)
        {
            if(score[i] > maxScore)
            {
                maxScore = score[i];
                Debug.Log("Score: " + score[i] + "/MaxScore: " + maxScore);
                playerWinner = i;
            }
        }
        Debug.Log(maxScore);
        for(int i = 0; i < 4; i++)
        {
            if(score[i] == maxScore)
            {
                counter++;
            }
        }
        if (counter != 1)
            return -1;
        if(currentScene == GenericFunctions.Scenes.EndGame)
            ResetScore();
        return playerWinner;
    }

    public void setPlayersAlive(int newAlive, int newDead)
    {
        playersAlive = newAlive;
        alivePlayer[newDead] = false;
        
        if (playersAlive <= 1)
        {
            if (playersAlive == 1)
            {
                lastScores = score;
                for (int i = 0; i < 4; i++)
                {
                    if (alivePlayer[i])
                    {
                        score[i]++;
                        Debug.Log("Score" + score[i]);
                        break;
                    }
                }
            }
            else
            {
                score = lastScores;
            }
            currentRound++;
             
            if(currentRound >= maxRound && getWinner() > -1)
            {
                ChangeScene((int)GenericFunctions.Scenes.EndGame);
            }
            else
            {
                endGame = true;
                end.SetActive(true);
                end.GetComponent<EndGame>().setEndGame(true);
                end.GetComponent<EndGame>().setScore(score);
            }
        }
        return;
    }

    public void ResetScore()
    {
        currentRound = 0;
        endGame = false;
        for (int i = 0; i < 4; i++)
        {
            score[i] = 0;
            lastScores[i] = 0;
        }
    }

    public int getPlayersAlive() { return playersAlive; }

    public void setPlayerKing(int newPlayer)
    {
        playerKing = newPlayer;
        return;
    }

    public int getPlayerKing() { return playerKing; }

}
