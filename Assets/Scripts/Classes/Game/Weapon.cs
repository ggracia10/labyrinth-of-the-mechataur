﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : MonoBehaviour {
    //! Use this for initialization
    public enum WeaponEnum { None, Sword, Pistol, Uzi, Shotgun, Hackgun, RocketLauncher, Weapon };
    //! Hasta Hackgun son armas de normal tier
    //! A partir de Hackgun para adelante son las armas de special tier
    //! RocketLauncher para arriba son armas exclusivas del mechatauro

    public WeaponEnum typeOfWeapon;
    public int randomTier = 0; //the random number that selects the tier (0 = normal, 1 = special)
    public int randomWeapon = 0; //the random number inside the tier selected
    public int weapInBasicTier = 0; //The number of weapons of the basic tier
    public int weapInSpecialTier = 0; //The number of weapons of the special tier
    void Start () {

        /* Esto lo comento porque es como se hará pero como de momento solo tenemos un arma
         * lo pondremos a pincho, cuando las tengamos todas descomentamos esta parte del texto
        randomTier = Random.Range(0, 10);
        if (randomTier >= 0 && randomTier < 8)
        {
            // como Random no coje el segundo numero, 
            // hacemos un random entre Sword (la primera arma) y la anterior a Hackgun
            // ya que hackgun es la primera arma del tier special Y SIEMPRE SERÁ ASÍ
            typeOfWeapon = (WeaponEnum)Random.Range((int)WeaponEnum.Sword, (int)WeaponEnum.Hackgun);
        }
        else if (randomTier >= 8)
        {
            // como Random no coje el segundo numero, 
            // hacemos un random entre Hackgun (la primera arma) y la anterior a RocketLauncher
            // ya que RocketLauncher es la primera arma del tier del mechatauro Y SIEMPRE SERÁ ASÍ
            typeOfWeapon = (WeaponEnum)Random.Range((int)WeaponEnum.Hackgun, (int)WeaponEnum.RocketLauncher);
        }*/
        randomWeapon = Random.Range(1, 6);
        switch (randomWeapon)
        {
            default:
                break;
            case 1:
                typeOfWeapon = WeaponEnum.Pistol;
                break;
            case 2:
                typeOfWeapon = WeaponEnum.Uzi;
                break;
            case 3:
                typeOfWeapon = WeaponEnum.Shotgun;
                break;
            case 4:
                typeOfWeapon = WeaponEnum.Sword;
                break;
            case 5:
                typeOfWeapon = WeaponEnum.RocketLauncher;
                break;
            case 6:
                typeOfWeapon = WeaponEnum.Weapon;
                break;
        }
	}
    private void Update()
    {
        GenericFunctions.setRenderOrder(gameObject);
    }
}
