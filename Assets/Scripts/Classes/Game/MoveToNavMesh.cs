﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveToNavMesh : MonoBehaviour {

    // Use this for initialization
    public Transform goal;
    NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        goal = GameObject.Find("Model0").GetComponent<Transform>();
        agent.destination = new Vector3(goal.position.x, transform.position.y, goal.position.y);
        
    }

    public void setGoal(Transform newGoal)
    {
        
        goal = newGoal;
        agent.destination = new Vector3(goal.position.x, transform.position.y, goal.position.y);
        /*if (transform.position.x > agent.destination.x + 0.5F ||
            transform.position.x < agent.destination.x - 0.5F ||
            transform.position.y > agent.destination.y + 0.5F ||
            transform.position.y < agent.destination.y - 0.5F)
        {
            goal = newGoal;
            agent.destination = new Vector3(goal.position.x, transform.position.y, goal.position.y);
        }*/
    }

    public void setPosition (Vector3 newPos)
    {
        gameObject.transform.position = new Vector3(newPos.x, transform.position.y, newPos.y);
    }

    public Vector3 getVelocity ()
    {
        return agent.velocity;
    }

    public void setSpeed(float newSpeed)
    {
        agent.speed = newSpeed;

        return;
    }
    public float getSpeed()
    {
        return agent.speed;
    }
}
