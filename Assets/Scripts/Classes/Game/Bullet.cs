﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Bullet : MonoBehaviour
{

    public struct BulletStruct
    {
        public string name;
        public float acceleration, distMax, speedMaxNum;
        public Vector2 speed;
        public float damage, speedIni;
        public BulletEnum typeOfBullet;
        public Entity.Direction direction;
    }
    infoBullet info;
    public enum BulletEnum { Weapon, Pistol, Uzi, Shotgun, Hackgun, RocketLauncher, None, Sword, Mechataur };
    public float maxTime;
    public float colTimer;
    public float temporizator;
    public float angle = 0;
    public float tempDeadBullet = 0;
    public bool exploded = false;
    public Vector3 currentPosition;
    public BulletStruct thisBullet;
    public Vector2 pos;
    string filePath, jsonString;
    public SpriteRenderer bulletSprite;

    //sounds
    public AudioClip bulletSound;
    public AudioSource bulletSoundSource;
    public Sprite sprite;

    //particles
    public ParticleSystem ExplosionParticle;
    TrailRenderer[] trails;
    int numtrails;

    private void Awake()
    {
        bulletSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;

        filePath = "";
        switch (gameObject.name)
        {
            case "AttackNone(Clone)":
                filePath = "./Files/Stats/Bullets/BulletNone.json";
                break;
            case "AttackWeapon(Clone)":
                filePath = "./Files/Stats/Bullets/BulletWeapon.json";
                break;
            case "AttackPistol(Clone)":
                filePath = "./Files/Stats/Bullets/BulletPistol.json";
                break;
            case "AttackUzi(Clone)":
                filePath = "./Files/Stats/Bullets/BulletUzi.json";
                break;
            case "AttackShotgun(Clone)":
                filePath = "./Files/Stats/Bullets/BulletShotgun.json";
                break;
            case "AttackSword(Clone)":
                filePath = "./Files/Stats/Bullets/BulletSword.json";
                break;
            case "AttackRocket(Clone)":
                filePath = "./Files/Stats/Bullets/BulletRocket.json";
                break;
        }
        jsonString = File.ReadAllText(filePath);
        info = JsonUtility.FromJson<infoBullet>(jsonString);
        thisBullet = new BulletStruct();
        thisBullet.name = info.name;
        thisBullet.acceleration = info.acceleration;
        thisBullet.distMax = info.distMax;
        thisBullet.speedMaxNum = info.speedMaxNum;
        thisBullet.damage = info.damage;
        maxTime = info.maxTime;
        colTimer = info.colTimer;
        thisBullet.speedIni = info.speedIni;
    }

    //!  Use this for initialization
    void Start()
    {
        bulletSprite = gameObject.GetComponent<SpriteRenderer>();
        switch (gameObject.name)
        {
            case "AttackNone(Clone)":
                thisBullet.typeOfBullet = BulletEnum.None;
                break;
            case "AttackWeapon(Clone)":
                thisBullet.typeOfBullet = BulletEnum.Weapon;
                break;
            case "AttackPistol(Clone)":
                thisBullet.typeOfBullet = BulletEnum.Pistol;
                break;
            case "AttackUzi(Clone)":
                thisBullet.typeOfBullet = BulletEnum.Uzi;
                break;
            case "AttackShotgun(Clone)":
                thisBullet.typeOfBullet = BulletEnum.Shotgun;
                break;
            case "AttackSword(Clone)":
                thisBullet.typeOfBullet = BulletEnum.Sword;
                break;
            case "AttackRocket(Clone)":
                thisBullet.typeOfBullet = BulletEnum.RocketLauncher;
                bulletSound = Resources.Load("Sounds/rocketLauncherBullet") as AudioClip;
                ExplosionParticle = gameObject.transform.GetChild(1).GetComponent<ParticleSystem>();
                ExplosionParticle.Stop();
                bulletSoundSource.PlayOneShot(bulletSound, 0.7F);
                break;
        }
        angle = ((angle * Mathf.PI) / 180);
        // Es para darle direccion a la bala, pero las estáticas no van a moverse así que lo ignoramos
        switch (thisBullet.direction) {

            default:
                break;
            case Entity.Direction.Down:
                transform.Rotate(0, 0, -90);
                break;
            case Entity.Direction.Left:
                transform.Rotate(0, 0, 180);
                break;
            case Entity.Direction.Up:
                transform.Rotate(0, 0, 90);
                break;
        }
        if(thisBullet.typeOfBullet != BulletEnum.None &&
            thisBullet.typeOfBullet != BulletEnum.Sword)
        {
            if (thisBullet.typeOfBullet == BulletEnum.RocketLauncher)
            {
                thisBullet.speedMaxNum = thisBullet.speedIni;
            }
            //Asignamos velocidad dependiendo del angulo que le pasemos por parametro
            switch (thisBullet.direction)
            {
                case Entity.Direction.Down:
                    thisBullet.speed = new Vector2(thisBullet.speedMaxNum * Mathf.Sin(angle),
                            -thisBullet.speedMaxNum * Mathf.Cos(angle));
                    
                    break;
                case Entity.Direction.Up:
                    thisBullet.speed = new Vector2(thisBullet.speedMaxNum * Mathf.Sin(angle),
                        thisBullet.speedMaxNum * Mathf.Cos(angle));
                    
                    break;
                case Entity.Direction.Right:
                    thisBullet.speed = new Vector2(thisBullet.speedMaxNum * Mathf.Cos(angle),
                        thisBullet.speedMaxNum * Mathf.Sin(angle));
             
                    break;
                case Entity.Direction.Left:
                    thisBullet.speed = new Vector2(-thisBullet.speedMaxNum * Mathf.Cos(angle),
                        thisBullet.speedMaxNum * Mathf.Sin(angle));
                    break;
            }
            currentPosition = gameObject.transform.position;
        }
        else
        {
            if (thisBullet.direction == Entity.Direction.Up)
            {

            }
        }
        
        pos = new Vector2(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y);
        temporizator = 0;
    }

    //!  Update is called once per frame
    void Update()
    {

        if (exploded)
        {
            tempDeadBullet += Time.deltaTime;
            if (tempDeadBullet >= 1F)
            {
                Destroy(gameObject);
            }
        }
        temporizator += Time.deltaTime;
        switch (thisBullet.typeOfBullet)
        {
            case BulletEnum.Pistol:
                //! code
                break;
            case BulletEnum.Shotgun:
                //! code
                break;
            case BulletEnum.Uzi:
                //! code
                break;
            case BulletEnum.Hackgun:
                //! code
                break;
            case BulletEnum.RocketLauncher:
                //! code
                break;
            case Bullet.BulletEnum.Sword:
                //code
                break;
            case BulletEnum.None:
                break;
            case BulletEnum.Weapon:
                break;
        }
        if (thisBullet.typeOfBullet == BulletEnum.RocketLauncher /*&& 
            thisBullet.speed.x < thisBullet.speedMaxNum &&
            thisBullet.speed.x > -thisBullet.speedMaxNum &&
            thisBullet.speed.y < thisBullet.speedMaxNum &&
            thisBullet.speed.y > thisBullet.speedMaxNum*/)
        {
            switch (thisBullet.direction)
            {
                case Entity.Direction.Down:
                    thisBullet.speed.y = (-thisBullet.acceleration * temporizator);
                    break;
                case Entity.Direction.Up:
                    thisBullet.speed.y = (thisBullet.acceleration * temporizator);
                    break;
                case Entity.Direction.Right:
                    thisBullet.speed.x = (thisBullet.acceleration * temporizator);
                    break;
                case Entity.Direction.Left:
                    thisBullet.speed.x = (-thisBullet.acceleration * temporizator);
                    break;
            }
        }
        gameObject.transform.localPosition +=
        new Vector3(thisBullet.speed.x, thisBullet.speed.y, gameObject.transform.position.z) *
        Time.deltaTime;
        if(gameObject.GetComponent<SpriteRenderer>() != null)
        {
            GenericFunctions.setRenderOrder(gameObject);
        }
        
        if (temporizator >= maxTime)
        {
            Explosion();
            
        }

    }

    public void Explosion()
    {
        if (exploded) return;
        switch (thisBullet.typeOfBullet)
        {
            case BulletEnum.Pistol:
                //! code
                Destroy(gameObject);
                break;
            case BulletEnum.Shotgun:
                //! code
                Destroy(gameObject);
                break;
            case BulletEnum.Uzi:
                //! code
                Destroy(gameObject);
                break;
            case BulletEnum.RocketLauncher:
                bulletSoundSource.Stop();
                if (!ExplosionParticle.isPlaying)
                {
                    ExplosionParticle.Play();
                }
                thisBullet.speed = new Vector2(0, 0);
                thisBullet.acceleration = 0;
                gameObject.GetComponent<BoxCollider2D>().size = new Vector2(15, 15);
                gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(0, 0);
                if(GameObject.Find("One shot audio") == null && bulletSoundSource.volume != 0)
                {
                    AudioSource.PlayClipAtPoint((Resources.Load("Sounds/rocketLauncherExplosion") as AudioClip), Camera.main.gameObject.transform.position, 1);
                }
                exploded = true;
                //! code
                break;
            case BulletEnum.Sword:
                //! Code
                Destroy(gameObject);
                break;
            case BulletEnum.None:
                //!Code
                Destroy(gameObject);
                break;
            case BulletEnum.Weapon:
                //!Code
                Destroy(gameObject);
                break;
            case BulletEnum.Hackgun:
                //!code
                Destroy(gameObject);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Lo hacemos así ya que en cuerpo a cuerpo no interesa que desaparezca si colisiona con una pared
        // ya que podria eliminarse sin crear colision en otro jugador
        if (thisBullet.typeOfBullet != BulletEnum.Hackgun &&
            thisBullet.typeOfBullet != BulletEnum.Sword &&
            thisBullet.typeOfBullet != BulletEnum.None && 
            thisBullet.typeOfBullet != BulletEnum.RocketLauncher)
        {
            if ((collision.gameObject.tag == "Wall" || collision.gameObject.name == "AttackSword(Clone)") && temporizator >= colTimer )
            {
                Destroy(gameObject);
            }
        }
        else if (thisBullet.typeOfBullet == BulletEnum.RocketLauncher)
        {
            if ((collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Player" || collision.gameObject.name == "AttackSword(Clone)"))
            {
                Explosion();
            }
        }
    }

    public void setNewSprite(Weapon.WeaponEnum newWeapon)
    {
        Debug.Log(newWeapon);
        //bulletSprite.sprite = Resources.Load<Sprite>("UI/WeaponHUD/" + newWeapon);
        sprite = Resources.Load<Sprite>("UI/WeaponHUD/" + newWeapon);
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        transform.localScale = new Vector3(0.5F, 0.5F, 1);
    }

}

public class infoBullet
{
    public string name;
    public float acceleration;
    public float distMax;
    public float speedMaxNum;
    public float damage;
    public float maxTime;
    public float colTimer;
    public float speedIni;
}







