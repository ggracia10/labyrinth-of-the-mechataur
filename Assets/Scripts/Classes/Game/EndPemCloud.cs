﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPemCloud : MonoBehaviour {


    private float TimerRound;
    private float maxTime;
    private float offsetCollider;
    private float sizeCollider;
    private BoxCollider2D[] arrayColliders = new BoxCollider2D [4];
    public CompositeCollider2D colliderTotal;
    private Vector2 tempOffset;
    private Vector2 tempSize;

    public List<Vector2> verts = new List<Vector2>();
    bool active;
    private bool endOfCount;

    //damage
    public float pemMaxDelayTime;
    public int pemDamage;


    Mesh mesh;

    public Vector3[] newVertices;
    public int[] newTriangles;
    MeshRenderer rendererMesh;
    ParticleSystem[] pemParticles = new ParticleSystem[4];
    ParticleSystem.ShapeModule[] pemShape = new ParticleSystem.ShapeModule[4];


    // Use this for initialization
    void Start (){
        rendererMesh = GetComponent<MeshRenderer>();
        active = false;
        /*Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.vertices = newVertices;
        mesh.triangles = newTriangles;*/
        newVertices = new Vector3[8];
        newTriangles = new int[24];


        colliderTotal = GetComponent<CompositeCollider2D>();
        mesh = new Mesh();
        CountVertices();
        
        
        GetComponent<MeshFilter>().mesh = mesh;
        //gameObject.GetComponent<Renderer>().sortingOrder = 5500;
        endOfCount = false;
        maxTime = 60;
        TimerRound = maxTime;
        sizeCollider = 2;
        offsetCollider = -sizeCollider / 2;
        tempOffset = new Vector2(offsetCollider, 0);
        tempSize = new Vector2(sizeCollider, 60);
        for (int i = 0; i < 4; i++)
        {
            pemParticles[i] = transform.GetChild(i).gameObject.transform.GetComponentInChildren<ParticleSystem>();
            pemShape[i] = pemParticles[i].shape;
            arrayColliders[i] = transform.GetChild(i).GetComponent<BoxCollider2D>();
            arrayColliders[i].offset = tempOffset;
            arrayColliders[i].size = tempSize;
            pemParticles[i].gameObject.SetActive(false);
        }
        //damage
        pemMaxDelayTime = 0.3F;
        pemDamage = 2;
        rendererMesh.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {

        if (!endOfCount)
        {
            TimerRound -= Time.deltaTime;

            if (TimerRound <= 0 && !active)
            {
                active = true;
                for (int i = 0; i < 4; i++)
                {
                    pemParticles[i].gameObject.SetActive(true);
                }
            }
            if(active) { 
                GetComponent<MeshRenderer>().enabled = true;
                
                sizeCollider += 0.5F*Time.deltaTime;
                if (sizeCollider >= 22.5F) {
                    sizeCollider = 22.5F;
                    endOfCount = true;
                }

                offsetCollider = -sizeCollider / 2;
                tempSize.x = sizeCollider;
                tempOffset.x = offsetCollider;


                //we assign the collider offset and the collider size for each child of P.E.M. Cloud
                CountVertices();
                for (int i = 0; i < 4; i++)
                {
                    arrayColliders[i] = transform.GetChild(i).GetComponent<BoxCollider2D>();
                    arrayColliders[i].offset = tempOffset;
                    arrayColliders[i].size = tempSize;
                    pemShape[i].scale = tempSize;
                    pemShape[i].position = tempOffset;
                }

            }
        }
    }

    void CountVertices()
    {
        verts.Clear();
        for (int i = 0; i < colliderTotal.pathCount; i++)
        {
            Vector2[] pathVerts = new Vector2[colliderTotal.GetPathPointCount(i)];
            colliderTotal.GetPath(i, pathVerts);
            verts.AddRange(pathVerts);
        }
        newVertices[0] = verts[9];
        newVertices[1] = verts[4];
        newVertices[2] = verts[3];
        newVertices[3] = verts[10];
        newVertices[4] = verts[14];
        newVertices[5] = verts[15];
        newVertices[6] = verts[12];
        newVertices[7] = verts[13];
        
        mesh.vertices = newVertices;
        
        mesh.triangles = new int[] {
            0,1,7, //yes
            7,3,0, //yes
            1,2,4, //yes
            1,4,7, //yes
            2,3,5, //yes
            5,4,2, //yes
            6,5,3, //yes
            7,6,3  //yes
        };
        newTriangles = mesh.triangles;
        
    }
    public bool getActive() { return active; }
}
