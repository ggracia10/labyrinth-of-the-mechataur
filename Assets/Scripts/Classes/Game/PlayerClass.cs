﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System.IO;
using UnityEngine.SceneManagement;

public class PlayerClass : Entity {
    
    int player;
    private Player playerInput;
    infoEntity info;
    public HUD hud;
    bool hitBullet = false;
    Weapon.WeaponEnum previousWeapon;
    public bool stunedBool = false;
    public bool pushed = false;
    public bool triggerBox = false;
    public Vector2 pushedVector = new Vector2(0, 0);
    PauseMenu pause;

    //time for traps
    private float delayTimeTrap;
    int counterTrap;

    //time for the PEM cloud
    private float delayPEM;
    
    //sounds
    public AudioClip shotSound;
    public AudioSource shotSoundSource;
    public AudioClip walkSound;
    public AudioSource walkSoundSource;

    //Particles
    public ParticleSystem deathParticle;
    public ParticleSystem ElectricParticle;
    private float deathTemp;
    private bool deathBool = false;
    private bool hitMechataur = false;
    public Vector3 centerMap;
    private Vector3 distHitMechataur;

    private float angleHitMechataur = 0;

    //GameState
    private GameState infoGame;

    private void Awake()
    {
        filePath = "./Files/Stats/PlayerStats.json";
        jsonString = File.ReadAllText(filePath);
        info = JsonUtility.FromJson<infoEntity>(jsonString);
        myStats = new Stats(info.name, info.maxHP, info.attack, info.defense, info.maxSpeed, 
            Weapon.WeaponEnum.None, 1, GenericFunctions.white);
        pause = GameObject.Find("PauseMenu").GetComponent<PauseMenu>();

        shotSound = Resources.Load("Sounds/pushSound") as AudioClip;
        shotSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        walkSound = Resources.Load("Sounds/walkSound") as AudioClip;
        walkSoundSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        walkSoundSource.clip = walkSound;
        walkSoundSource.loop = true;
        centerMap = GameObject.Find("Spawner0").GetComponent<Transform>().position;
    }
    
    //!  Use this for initialization
    void Start()
    {
        infoGame = GameObject.Find("GameState").GetComponent<GameState>();
        //particles
        timeRocketImpact = 0;
        timeMaxRocketImpact = 5;
        ElectricParticle = gameObject.transform.GetChild(7).GetComponent<ParticleSystem>();
        ElectricParticle.Stop();
        deathParticle = gameObject.transform.GetChild(5).GetComponent<ParticleSystem>();
        deathParticle.Stop();
        deathTemp = 1;

        mSpriteRender = gameObject.GetComponent<SpriteRenderer>();
        currentSpeedX = 0;
        currentSpeedY = 0;
        currentAngle = 0;
        changeAnim = false;
        moving = false;
        //Debug.Log(myStats);
        currentDirection = Direction.Down;
        lastDirection = currentDirection;
        trans = gameObject.transform;
        anim = gameObject.GetComponent<Animator>();
        animSkin = gameObject.transform.GetChild(0).GetComponent<Animator>();
        animWeapon = gameObject.transform.GetChild(6).GetComponent<Animator>();
        attacking = false;
        timeStuned = 0;
        timeStunedMax = 1;
        switch (gameObject.name)
        {
            case "Model0":
                player = 0;
                break;
            case "Model1":
                player = 1;
                break;
            case "Model2":
                player = 2;
                break;
            case "Model3":
                player = 3;
                break;
        }
        myStats.color = infoGame.getPlayerColors(player);
        myStats.name = "Model";
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        playerInput = ReInput.players.GetPlayer(player);
        mSpriteRender.color = myStats.color;
        hud = GameObject.Find("HUD" + player).GetComponent<HUD>();
        hud.Init(myStats.color, myStats.hpMax, myStats.skin);
        tempInvencible = 0;
        tempInvencibleMax = 1;
        ChangeWeapon(Weapon.WeaponEnum.None);
        inATrap = false;

        for (int i = 0; i < (int)Bullet.BulletEnum.Sword; i++)
        {
            Bullet.BulletEnum bulletTemp;
            bulletTemp = (Bullet.BulletEnum)i;
            
            GameObject attack;
            if (bulletTemp == Bullet.BulletEnum.Hackgun)
            {
                continue;
            }
            else if(bulletTemp == Bullet.BulletEnum.RocketLauncher)
            {
                attack = Instantiate(Resources.Load<GameObject>("Attack/AttackRocket"));
                attack.transform.position = new Vector3(-100 - (i * 10), 0, 0);
                attack.GetComponent<Bullet>().bulletSoundSource.volume = 0;
            }
            else
            {
                attack = Instantiate(Resources.Load<GameObject>("Attack/Attack" + bulletTemp));
                attack.transform.position = new Vector3(-100 - (i * 10), 0, 0);
            }
            
            
        }
        return;
    }
    
    //!  Update is called once per frame
    void Update()
    {
        if(infoGame.endGame)
        {
            return;
        }
        if(infoGame.introCamera)
        {
            return;
        }
        if (timeRocketImpact < timeMaxRocketImpact)
        {
            timeRocketImpact += Time.deltaTime;
        }
        if(Input.GetKeyDown(KeyCode.F12))
        {
            SceneManager.LoadScene("Arena");
            
        }
        if (deathBool)
        {
            Die();
            return;
        }
        hitBullet = false;
        if (pause.getPause())
        {
            CheckControls();
            return;
        }
        if (currentCoolDown != coolDown)
        {
            currentCoolDown += Time.deltaTime;
            if (currentCoolDown >= coolDown)
            {
                currentCoolDown = coolDown;
            }
        }
        tempInvencible += Time.deltaTime;
        ResetVariables();
        if(!stunedBool)
        {
            CheckControls();
            Movement(currentSpeedX, currentSpeedY, currentAngle, moving);
        }
        else
        {
            timeStuned += Time.deltaTime;
            if (pushed)
            {
                tempInvencible = 0;
                float desaccel = 2F;
                if (pushedVector.x > 0)
                {
                    pushedVector -= new Vector2(desaccel, 0);
                }
                else if (pushedVector.x < 0)
                {
                    pushedVector -= new Vector2(-desaccel, 0);
                }
                else if (pushedVector.y > 0)
                {
                    pushedVector -= new Vector2(0, desaccel);
                }
                else if (pushedVector.y < 0)
                {
                    pushedVector -= new Vector2(0, -desaccel);
                }

                if (pushedVector == new Vector2(0, 0))
                {
                    pushed = false;
                    stunedBool = false;
                    timeStuned = 0;
                    pushedVector = new Vector2(0, 0);
                }
                Movement(pushedVector.x, pushedVector.y, 0, true);
            }
            if(hitMechataur)
            {
                //Debug.Log(timeStuned);
                //Movement(25, 25, angleHitMechataur, true);
                MovementHitMechataur(distHitMechataur.x, distHitMechataur.y, angleHitMechataur);
            }
            if (timeStuned >= timeStunedMax)
            {
                
                
                stunedBool = false;
                pushed = false;
                timeStuned = 0;
                pushedVector = new Vector2(0, 0);
                if (hitMechataur)
                {
                    stunedBool = true;
                    hitMechataur = false;
                }
            }
        }
        
        ChangeAnimation(currentSpeedX, currentSpeedY, attacking, (int)myStats.currentWeapon, stuned, changeAnim,
            (int) currentDirection, myStats);
        lastDirection = currentDirection;
    }

    //! Check controls of the player.
    protected void CheckControls()
    {
     
        if (currentCoolDown >= coolDown)
        {
            if(myStats.currentWeapon != Weapon.WeaponEnum.Uzi)
            {
                if (playerInput.GetButtonDown("Attack"))
                {
                    Attack(myStats);
                    
                    currentCoolDown = 0;
                }
            }
            else
            {
                if (playerInput.GetButton("Attack"))
                {
                    Attack(myStats);
                    
                    currentCoolDown = 0;
                }
            }
            if (playerInput.GetButtonDown("Pick") && myStats.currentWeapon != Weapon.WeaponEnum.None)
            {
                previousWeapon = myStats.currentWeapon;
                ChangeWeapon(Weapon.WeaponEnum.Weapon);
                Attack(myStats);
            }

        }

        //the pick weapon input is in OnTriggerStay2D
        
        if (playerInput.GetButtonDown("Pause"))
        {
            pause.setPause(!pause.getPause());
            
        }

        if (playerInput.GetAxis("MoveX") < -0.3F || playerInput.GetButton("MoveLeft"))
        {
            currentSpeedX = -myStats.maxSpeed;
            moving = true;
        }
        else if (playerInput.GetAxis("MoveX") > 0.3F || playerInput.GetButton("MoveRight"))
        {
            currentSpeedX = myStats.maxSpeed;
            moving = true;
        }
        if (playerInput.GetAxis("MoveY") < -0.3F || playerInput.GetButton("MoveDown"))
        {
            currentSpeedY = -myStats.maxSpeed;
            moving = true;
        }
        else if (playerInput.GetAxis("MoveY") > 0.3F || playerInput.GetButton("MoveUp"))
        {
            currentSpeedY = myStats.maxSpeed;
            moving = true;
        }
        
         if (currentSpeedX != 0 && currentSpeedY != 0)
        {
            currentAngle = Mathf.PI / 4;
        }

        /*currentSpeedX = myStats.maxSpeed * playerInput.GetAxis("MoveX");
        currentSpeedY = myStats.maxSpeed * playerInput.GetAxis("MoveY");*/
        if(currentSpeedX != 0 || currentSpeedY != 0)
        {
            moving = true;
            if(!walkSoundSource.isPlaying)
            {
                walkSoundSource.Play();
            }
        }
        else
        {
            walkSoundSource.Stop();
        }
        return;
    }

    //! This function set Position and Direction to an instance of a bullet
    void setDirectionBullet(Vector2 newVect, Vector3 newPos, GameObject newAttack, Stats newStats)
    {
        if (myStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None)
        {
            newAttack.GetComponent<BoxCollider2D>().size = new Vector2(1f, 1.25F);
            newAttack.GetComponent<BoxCollider2D>().offset = new Vector2(0.5f, 0F);
        }
        float offsetS = newAttack.GetComponent<Collider2D>().bounds.size.x / 2F;
        switch (currentDirection)
        {
            case Direction.Down:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(2).transform.position - new Vector3(0, 1F, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Down;
                if (myStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None)
                {
                    newAttack.GetComponent<BoxCollider2D>().offset = new Vector2(0.4f, 0F);
                    newAttack.GetComponent<BoxCollider2D>().size = new Vector2(0.9f, 1.25F);
                }
                break;
            case Direction.Up:

                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(1).transform.position + new Vector3(0, 3F, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Up;
                if (myStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None)
                {
                    newAttack.GetComponent<BoxCollider2D>().size = new Vector2(1.1f, 1.2F);
                    newAttack.GetComponent<BoxCollider2D>().offset = new Vector2(0.21f, 0F);
                }
                break;
            case Direction.Left:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(3).transform.position - new Vector3(0.5F, 0, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Left;
                break;
            case Direction.Right:
                newVect = new Vector2(offsetS, 0);
                newPos = gameObject.transform.GetChild(4).transform.position + new Vector3(1F, 0, 0);
                newAttack.GetComponent<Bullet>().thisBullet.direction = Direction.Right;
                break;
        }
        if(!((newStats.currentWeapon == Weapon.WeaponEnum.Sword || myStats.currentWeapon == Weapon.WeaponEnum.None) && 
            currentDirection == Direction.Up))
        {
            newAttack.GetComponent<Collider2D>().offset = newVect;
        }
        
        if (newStats.currentWeapon == Weapon.WeaponEnum.None ||
            newStats.currentWeapon == Weapon.WeaponEnum.Sword)
        {
            newAttack.GetComponent<Transform>().SetParent(gameObject.transform);
        }
        newAttack.GetComponent<Transform>().position = newPos;
    }

    override protected void Attack(Stats tempStats)
    {
        if (myStats.ammoCount <= 0 && tempStats.currentWeapon != Weapon.WeaponEnum.None)
        {
            shotSound = Resources.Load("Sounds/noAmmo") as AudioClip;
            shotSoundSource.PlayOneShot(shotSound, 0.7F);
            return;
        }
        //Debug.Log(tempStats.name + ": " + tempStats.currentWeapon);
        string pathAttack = "";
        switch (tempStats.currentWeapon)
        {
            case Weapon.WeaponEnum.None:
                pathAttack = "Attack/AttackNone";
                shotSound = Resources.Load("Sounds/pushShot") as AudioClip;
                break;
            case Weapon.WeaponEnum.Weapon:
                pathAttack = "Attack/AttackWeapon";
                shotSound = Resources.Load("Sounds/pushShot") as AudioClip;
                break;
            case Weapon.WeaponEnum.Pistol:
                pathAttack = "Attack/AttackPistol";
                shotSound = Resources.Load("Sounds/pistolShot") as AudioClip;
                break;
            case Weapon.WeaponEnum.Uzi:
                pathAttack = "Attack/AttackUzi";
                shotSound = Resources.Load("Sounds/uziShot") as AudioClip;
                break;
            case Weapon.WeaponEnum.Shotgun:
                pathAttack = "Attack/AttackShotgun";
                shotSound = Resources.Load("Sounds/shotgunShot") as AudioClip;
                break;
            case Weapon.WeaponEnum.Sword:
                pathAttack = "Attack/AttackSword";
                shotSound = Resources.Load("Sounds/pushSound") as AudioClip;
                break;
            case Weapon.WeaponEnum.RocketLauncher:
                pathAttack = "Attack/AttackRocket";
                shotSound = Resources.Load("Sounds/rocketLauncherShot") as AudioClip;
                stunedBool = true;
                pushed = true;
                float pushedSpeed = 35;
                switch (currentDirection)
                {
                    case Direction.Left:
                        pushedVector = new Vector2(pushedSpeed, 0);
                        break;
                    case Direction.Right:
                        pushedVector = new Vector2(-pushedSpeed, 0);
                        break;
                    case Direction.Down:
                        pushedVector = new Vector2(0, pushedSpeed);
                        break;
                    case Direction.Up:
                        pushedVector = new Vector2(0, -pushedSpeed);
                        break;
                }
                break;
        }
        Vector2 tempVect = new Vector2(0, 0);
        Vector3 tempPosition = new Vector3(0, 0, 0);
        myStats.ammoCount -= 1;
        shotSoundSource.PlayOneShot(shotSound, 0.7F);

        if (tempStats.currentWeapon == Weapon.WeaponEnum.Shotgun)
        {
            for (int i = 0; i < 5; i++)
            {
                GameObject attackS = Instantiate(Resources.Load<GameObject>(pathAttack));
                Bullet newBullet = attackS.GetComponent<Bullet>();
                newBullet.angle = -20 + (i * 10F);
                setDirectionBullet(tempVect, tempPosition, attackS, tempStats);
            }
        }
        else
        {
            GameObject attack = Instantiate(Resources.Load<GameObject>(pathAttack));
            if (tempStats.currentWeapon == Weapon.WeaponEnum.Weapon)
            {
                Debug.Log(previousWeapon);
                attack.GetComponent<Bullet>().setNewSprite(previousWeapon);
                ChangeWeapon(Weapon.WeaponEnum.None);
            }
            setDirectionBullet(tempVect, tempPosition, attack, tempStats);
            
        }
        //! code
        return;
    }

    override public void Stunned(Bullet.BulletEnum typeBullet, Direction newDir, float newDamage)
    {
        if (infoGame.getPlayersAlive() <= 1)
            return;
        if (myStats.hp <= 0)
        {
            return;
        }
        switch (typeBullet)
        { //Only will stune (push the player) the shotgun, none and sword weapons 
            
            case Bullet.BulletEnum.Pistol:
                myStats.hp -= (int)newDamage;
                break;
            case Bullet.BulletEnum.Shotgun:
                //code
                myStats.hp -= (int)newDamage;
                stunedBool = true;
                PushedFunction(newDir, typeBullet);
                break;
            case Bullet.BulletEnum.Uzi:
                myStats.hp -= (int)newDamage;
                break;
            case Bullet.BulletEnum.RocketLauncher:
                myStats.hp -= (int)newDamage;
                break;
            case Bullet.BulletEnum.Hackgun:
                //code
                break;
            case Bullet.BulletEnum.Sword:
                myStats.hp -= (int)newDamage;
                stunedBool = true;
                PushedFunction(newDir, typeBullet);
                AudioSource.PlayClipAtPoint((Resources.Load("Sounds/punchSound") as AudioClip), Camera.main.gameObject.transform.position, 1);
                break;
            case Bullet.BulletEnum.None:
                stunedBool = true;
                PushedFunction(newDir, typeBullet);
                if (gameObject.transform.Find("Crown"))
                {
                    gameObject.transform.Find("Crown").transform.parent = null;
                }
                break;
            case Bullet.BulletEnum.Weapon:
                Debug.Log("AAAAAAAAAAa");
                stunedBool = true;
                PushedFunction(newDir, typeBullet);
                break;
            case Bullet.BulletEnum.Mechataur:
                if(!hitMechataur)
                myStats.hp -= (int)newDamage;
                HitMechataur();
                break;
            default:
                Debug.Log("ERROR");
                break;
        }

        GameObject.Find("HUD" + player).GetComponent<HUD>().SetHealth(myStats.hp);

        if (myStats.hp <= 0)
        {
            GameObject.Find("Main Camera").GetComponent<CameraControl>().setShaking(true);
            deathBool = true;
            
            //gameObject.transform.GetChild(x).GetComponent<GameObject>().SetActive(false);     x = weaponSlot;
        }
        return;
    }

    protected void HitMechataur()
    {
            stunedBool = true;
            timeStuned = -0.5F;
            distHitMechataur = centerMap - trans.position;
            angleHitMechataur = Mathf.Atan2(distHitMechataur.y, distHitMechataur.x);
            hitMechataur = true;
    }

    protected void MovementHitMechataur(float speedX, float speedY, float newAngle)
    {
        //Animacion de que se hace mas grande y luego mas pequeño, sensacion de que va hacia arriba
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = 7000;
        if (timeStuned < 0.25F)
        {
            trans.localScale += 0.4F * new Vector3(1, 1, 0) * Time.deltaTime;
        }
        else if (timeStuned < 1 && timeStuned > 0.25F)
        {
            trans.localScale -= 0.4F * new Vector3(1, 1, 0) * Time.deltaTime;
        }
        if (trans.position.x < centerMap.x - 1 || trans.position.x > centerMap.x + 1 ||
            trans.position.y < centerMap.y - 1 || trans.position.y > centerMap.y + 1)
        {
            trans.position += new Vector3(speedX, speedY, trans.position.z) * Time.deltaTime;
        }
        

        else
        {
            trans.localScale = new Vector3(0.75F, 0.75F, 0.75F);
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = 7000;
        }
    }

    override protected void TrapHit(Trap.TrapsEnum trapType, bool newStunned, float newDamage)
    {
        if (infoGame.getPlayersAlive() <= 1)
            return;
        if (myStats.hp <= 0)
        {
            return;
        }
        myStats.hp -= (int)newDamage;
        GameObject.Find("HUD" + player).GetComponent<HUD>().SetHealth(myStats.hp);
        if (newStunned)
        {
            stunedBool = true;
        }

        if (myStats.hp <= 0)
        {
            deathBool = true;
            GameObject.Find("Main Camera").GetComponent<CameraControl>().setShaking(true);
        }
        return;
    }

    void ChangeWeapon(Weapon.WeaponEnum newWeapon)
    {
        myStats.currentWeapon = newWeapon;
        currentCoolDown = 0;
        switch(newWeapon)
        {
            case Weapon.WeaponEnum.None:
                coolDown = 1;
                myStats.ammoCount = 2;
                break;
            case Weapon.WeaponEnum.Weapon:
                coolDown = 1;
                myStats.ammoCount = 1;
                break;
            case Weapon.WeaponEnum.Sword:
                coolDown = 1; //Provisional
                myStats.ammoCount = 10;
                break;
            case Weapon.WeaponEnum.Pistol:
                coolDown = 0.1F; //Provisional
                myStats.ammoCount = 25;
                break;
            case Weapon.WeaponEnum.Uzi:
                coolDown = 0.05F; //Provisional
                myStats.ammoCount = 50;
                break;
            case Weapon.WeaponEnum.Shotgun:
                coolDown = 1; //Provisional
                myStats.ammoCount = 8;
                break;
            case Weapon.WeaponEnum.Hackgun:
                coolDown = 1; //Provisional
                break;
            case Weapon.WeaponEnum.RocketLauncher:
                coolDown = 1; //Provisional
                myStats.ammoCount = 3;
                break;
            default:
                Debug.Log("ERROR");
                break;
        }
        hud.ChangeWeapon(newWeapon);
    }

    override public void Die()
    {
        /*foreach (Joystick j in playerInput.controllers.Joysticks)
        {
            playerInput.controllers.Joysticks[player].SetVibration(0, 0.5F, 1); //ya veremos cuanto
        }*/
        if(!deathParticle.isPlaying)
        {
            deathParticle.Play();
        }
        deathTemp -= Time.deltaTime;
        if (deathTemp < 0.5F) {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        }

        if (deathTemp <= 0)
        {
            infoGame.setPlayersAlive(infoGame.getPlayersAlive() - 1, player);
            gameObject.SetActive(false);
        }
        
        
        return;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            //Debug.Log("TAG: " + collision.gameObject.tag);
            Bullet bulletCol = collision.gameObject.GetComponent<Bullet>();
            //Debug.Log("Bool = " + hitBullet);
            if (!hitBullet || bulletCol.thisBullet.typeOfBullet == Bullet.BulletEnum.Shotgun)
            {
                if (bulletCol.thisBullet.typeOfBullet == Bullet.BulletEnum.RocketLauncher)
                {
                    if (timeRocketImpact < timeMaxRocketImpact) return;
                    timeRocketImpact = 0;
                }
                hitBullet = true;
                Stunned(bulletCol.thisBullet.typeOfBullet, bulletCol.thisBullet.direction, bulletCol.thisBullet.damage);
                bulletCol.GetComponent<Bullet>().Explosion();
            }
            //Debug.Log("Type: " + bulletCol.thisBullet.typeOfBullet);

        }

        if (collision.gameObject.tag == "Trap")
        {
            if (!inATrap)
            {
                inATrap = true;
                Trap trapCol = collision.gameObject.GetComponent<Trap>();
                if (trapCol.trapType == Trap.TrapsEnum.SPIKES)
                {
                    trapCol.changeStatus(Trap.TrapStatus.Loading);
                }
                delayTimeTrap = 0;
            }
            
        }
        if(collision.gameObject.tag == "PEMCloud")
        {
            if (GameObject.Find("P.E.M Minicloud").GetComponent<EndPemCloud>().getActive())
            {
                ElectricParticle.Play();
                delayPEM = 0;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Trap")
        {
            inATrap = false;
        }
        if (collision.gameObject.tag == "PEMCloud")
        {
            ElectricParticle.Stop();
            delayPEM = 0;
            
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Crown"){
            if (playerInput.GetButtonDown("Pick") && collision.transform.parent == null)
            {
                collision.transform.parent = gameObject.transform;
                collision.transform.localPosition = new Vector3(0, 2.47F, 0);
            }
        }
        if (collision.gameObject.tag == "DropWeapon")
        {
            if (playerInput.GetButtonDown("Pick"))
            {
                ChangeWeapon(collision.GetComponent<Weapon>().typeOfWeapon);
                Destroy(collision.gameObject);
            }
        }
        //collisions with the type of traps
        if (collision.gameObject.tag == "Trap")
        {
            
            bool isStunned = false;
            Trap trapCol = collision.gameObject.GetComponent<Trap>();

            if (trapCol.damageActive)
            {
                delayTimeTrap -= Time.deltaTime;
                if (delayTimeTrap <= 0)
                {
                    delayTimeTrap = trapCol.maxdelayTime;
                    switch (trapCol.trapType)
                    {
                        case Trap.TrapsEnum.SPIKES:
                            delayTimeTrap = 5*trapCol.maxdelayTime;
                            isStunned = true;
                           
                            break;

                        case Trap.TrapsEnum.PEM:
                            
                            break;
                        default:
                            Debug.Log("ERROR");
                            break;
                    }
                    TrapHit(trapCol.trapType, isStunned, trapCol.damageperdelay);
                   
                }
            }     
        }

        if (collision.gameObject.tag == "PEMCloud")
        {
            EndPemCloud pemCol = collision.GetComponent<EndPemCloud>();
            if (pemCol.getActive())
            {
                delayPEM -= Time.deltaTime;

                if (delayPEM <= 0)
                {
                    delayPEM = pemCol.pemMaxDelayTime;
                    if(myStats.hp <= 0)
                    {
                        return;
                    }
                    myStats.hp -= pemCol.pemDamage;
                    GameObject.Find("HUD" + player).GetComponent<HUD>().SetHealth(myStats.hp);
                    if (myStats.hp <= 0)
                    {
                        deathBool = true;
                        GameObject.Find("Main Camera").GetComponent<CameraControl>().setShaking(true);
                    }
                }
            }

        }
    }

    protected void PushedFunction(Direction newDir, Bullet.BulletEnum typeBullet)
    {
        pushed = true;
        
        int pushedSpeed = 0;
        if (typeBullet == Bullet.BulletEnum.None ||
            typeBullet == Bullet.BulletEnum.Sword ||
            typeBullet == Bullet.BulletEnum.Weapon)
        {
            pushedSpeed = 40;
        }
        else if(typeBullet == Bullet.BulletEnum.Shotgun)
        {
            pushedSpeed = 30;
        }
        switch (newDir)
        {
            case Direction.Left:
                pushedVector = new Vector2(-pushedSpeed, 0);
                break;
            case Direction.Right:
                pushedVector = new Vector2(pushedSpeed, 0);
                break;
            case Direction.Down:
                pushedVector = new Vector2(0,-pushedSpeed);
                break;
            case Direction.Up:
                pushedVector = new Vector2(0, pushedSpeed);
                break;
        }
    }

}


