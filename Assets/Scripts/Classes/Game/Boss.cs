﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
//!!!!!!!!!!!! error en el currenttimerush
public class Boss : Entity {
    //declaracion de variables
    protected bool followingTarget;
    protected bool rushing,runRush,chargingRush,PEMactive;
    protected int currentTarget;
    protected float currentTimeFollowing, maxTimeFollowing, maxTimeRushCharge, currentTimeRushCharge,
        currentTimeRushing, maxTimeRushing, actualSpeed, timeWaitingRush, timeMaxWaitingRush;
    protected GameObject targetPlayer;
    infoEntity info;
    public MoveToNavMesh pathFinder;
    protected Vector3 targetPosition;
    float timeUpdateTarget, timeMaxUpdateTarget;
    Vector3 lastPosition, currentPosition;
    Vector2 rushSpeed;
    Vector2 rushDistance;
    float rushAngle;
    //---------------

    private void Awake()
    {
        filePath = "./Files/Stats/MechaStats.json";
        jsonString = File.ReadAllText(filePath);
        info = JsonUtility.FromJson<infoEntity>(jsonString);
        //stats
        myStats = new Stats(info.name, info.maxHP, info.attack, info.defense, info.maxSpeed,
            Weapon.WeaponEnum.None, 1, GenericFunctions.white);
        
        
        

    }
    //!  Use this for initialization
    void Start () {
        //sprite/animations
        mSpriteRender = gameObject.GetComponent<SpriteRenderer>();
        changeAnim = false;
        anim = gameObject.GetComponent<Animator>();
        mSpriteRender.color = myStats.color;

        //rush
        rushAngle = 0;
        chargingRush = false;
        rushing = false;
        runRush = false;
        maxTimeRushCharge = 1;
        maxTimeRushing = 0.7F;
        currentTimeRushing = 0;
        currentTimeRushCharge = 0;
        timeWaitingRush = 0;
        timeMaxWaitingRush = 3;
        //
        //speed
        actualSpeed = myStats.maxSpeed;
        currentSpeedX = 0;
        currentSpeedY = 0;
        //
        //direction
        currentAngle = 0;
        currentDirection = Direction.Down;
        lastDirection = currentDirection;
        //
        //control movimiento
        moving = false;// saber si se esta moviendo o no
        //
        trans = gameObject.transform;
        attacking = false;
        //follow 
        followingTarget = false;
        maxTimeFollowing = 15;
        currentTarget = -1;
        timeMaxUpdateTarget = 0F;
        timeUpdateTarget = timeMaxUpdateTarget;              
        targetPlayer = GameObject.Find("Model1");
        PEMactive = true;
        //path finder
        pathFinder = GameObject.Find("ObjectMesh").GetComponent<MoveToNavMesh>();
        pathFinder.setSpeed(actualSpeed);
        SetTarget();
        
        //
    }

    void TimeUpdate()
    {
        currentTimeFollowing += Time.deltaTime;
        if(timeWaitingRush < timeMaxWaitingRush && !rushing && !chargingRush)
        {
            timeWaitingRush += Time.deltaTime;
        }
        if (currentTimeFollowing >= maxTimeFollowing)
        {
            
            currentTimeFollowing = 0;
            followingTarget = false;
        }
        setPostionTarget();
    }

    //!  Update is called once per frame
    void FixedUpdate() {
        
        ResetVariables();
        lastPosition = currentPosition;
        currentPosition = gameObject.transform.position;

        if(!targetPlayer.activeSelf)
        {
            SetTarget();
        }

        //Debug.Log("rushing: " + rushing);
        //Debug.Log("chargerushing: " + chargingRush);
        PEMactive = GameObject.Find("P.E.M Minicloud").GetComponent<EndPemCloud>().getActive();
        if (PEMactive)
        {
            targetPlayer = GameObject.Find("MechaHouse");
        }
        if (!rushing&&!chargingRush) {
            //Debug.Log("Holicaracoli");
            TimeUpdate();
            if (!followingTarget)
            {
                SetTarget();
                currentTimeFollowing = 0;
            }
            if (!PEMactive && timeWaitingRush >= timeMaxWaitingRush)
            {
                for (int i = 0; i < 4; i++)
                {
                    if(GameObject.Find("Model"+i) != null)
                    {
                        CheckForRush(GameObject.Find("Model" + i));
                    }
                    
                }
                
            }
            /***********************************************************/
            if (transform.position.x > targetPlayer.transform.position.x + 0.2f ||
                transform.position.x < targetPlayer.transform.position.x - 0.2f ||
                transform.position.y > targetPlayer.transform.position.y + 0.2f ||
                transform.position.y < targetPlayer.transform.position.y - 0.2f)
            {
                pathFinder.setSpeed(myStats.maxSpeed);
                setDirectionSprite();
                gameObject.transform.position =
                new Vector3(pathFinder.transform.position.x, pathFinder.transform.position.z + 0.85F,
                                gameObject.transform.position.z);
            }
            else
            {
                if(timeWaitingRush < 5)
                {
                    timeWaitingRush = timeMaxWaitingRush - 5.5F;
                }
                pathFinder.setPosition(trans.position);
            }
            
        }
        else if (chargingRush)
        {
            timeWaitingRush = 0;
            //Debug.Log("Charging time = " + currentTimeRushCharge + "/" + maxTimeRushing);
            
            ChargeRush(rushDistance);
        }
        else if(rushing)
        {
            Rush();
        }

        ChangeAnimation(currentSpeedX, currentSpeedY, attacking, (int)myStats.currentWeapon, stuned, 
            changeAnim, (int)currentDirection, myStats);
        GenericFunctions.setRenderOrder(gameObject);
        
    }

    void setDirectionSprite()
    {
        if (Mathf.Abs(pathFinder.getVelocity().x) > Mathf.Abs(pathFinder.getVelocity().z))
        {
            if (pathFinder.getVelocity().x > 0)
            {
                currentDirection = Direction.Right;
                currentSpeedX = myStats.maxSpeed;
            }
            else
            {
                currentDirection = Direction.Left;
                currentSpeedX = -myStats.maxSpeed;
            }
        }
        else if (Mathf.Abs(pathFinder.getVelocity().z) > Mathf.Abs(pathFinder.getVelocity().x))
        {
            if (pathFinder.getVelocity().z > 0)
            {
                currentDirection = Direction.Up;
                currentSpeedY = myStats.maxSpeed;
            }
            else
            {
                currentDirection = Direction.Down;
                currentSpeedY = -myStats.maxSpeed;
            }
        }
    }
    //attack functions
    void CheckForRush(GameObject newPlayer)
    {
        //Debug.Log("I'm on the checkRush");
        float distXTemp = newPlayer.GetComponent<Transform>().position.x - gameObject.transform.position.x;
        float distYTemp = newPlayer.GetComponent<Transform>().position.y - gameObject.transform.position.y;
        Vector2 distOfRush = new Vector2(distXTemp, distYTemp);
        if ((distXTemp < 3.00F && distXTemp > -3.00F) || (distYTemp < 3.00F && distYTemp > -3.00F))
        {
            if (!CheckRushCollision(gameObject.transform.position, distOfRush) && !rushing)
            {
                //Debug.Log("I will chargeRush");
                chargingRush = true;
                rushing = false;
                rushDistance = distOfRush;
            }
        }
    }

    void ChargeRush(Vector2 dist)
    {
        currentTimeRushCharge += Time.deltaTime;
        if (currentTimeRushCharge >= maxTimeRushCharge)
        {
            chargingRush = false;
            currentTimeRushCharge = 0;
            rushing = true;
            //Debug.Log("rushing = true");
            return;
        }
        if (dist.x > 0)
        {
            currentSpeedX = 20;
        }
        else
        {
            currentSpeedX = -20;
        }
        if (dist.y > 0)
        {
            currentSpeedY = 20;
        }
        else
        {
            currentSpeedY = -20;
        }

        rushSpeed = new Vector2(currentSpeedX, currentSpeedY);
        rushAngle = Mathf.Atan2(Mathf.Abs(dist.y), Mathf.Abs(dist.x));
    }

    bool CheckRushCollision(Vector2 newVect, Vector2 newDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(newVect, newDir, 3.00F);
        if (hit)
        {
            if (hit.collider.gameObject.tag == "Wall")
            {
                Debug.Log("SAD");
                return true;
            }            
        }
        return false;
    }

    void Rush()
    {
        currentTimeRushing += Time.deltaTime;
        if (currentTimeRushing >= maxTimeRushing)
        {
            stopRush();
            return;
        }
        //Debug.Log("Speed: " + rushSpeed);
        
        Movement(rushSpeed.x, rushSpeed.y, rushAngle, true);
    }
    void stopRush()
    {
        pathFinder.setPosition(trans.position);
        rushing = false;
        chargingRush = false;
        currentTimeRushing = 0;
        currentTimeRushCharge = 0;
        SetTarget();
    }
    //
    //! To set a new target to follow
    void SetTarget()
    {
        Debug.Log("I'm changing the target");
        currentTarget = -1;
        do
        {
           
            currentTarget = Random.Range(0, 4);
        } while (GameObject.Find("Model" + currentTarget) == null);
        
        targetPlayer = GameObject.Find("Model" + currentTarget);
        followingTarget = true;
        if (targetPlayer != null)
        {
            setPostionTarget();
        }
        return;
    }

    void setPostionTarget()
    {
        pathFinder.setGoal(targetPlayer.transform);
        
    }

    void SetSpeed(float newSpeed)
    {
        actualSpeed = newSpeed;
        return;
    }

    //! To follow the target
   

    override protected void Attack(Stats tempStats)
    {
        //! code
        return;
    }
    bool CheckToAttack()
    {
        bool returnAttack = false;
        //! code
        return returnAttack;
    }
    override public void Stunned(Bullet.BulletEnum typeBullet, Direction newDir, float newDamage)
    {
        //! The code
        return;
    }
    override public void Die()
    {
        return;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            targetPlayer = collision.gameObject;
            setPostionTarget();
            rushing = true;
            Rush();
            collision.gameObject.GetComponent<PlayerClass>().Stunned(Bullet.BulletEnum.Mechataur, Direction.None, 25);
        }
    }
}
