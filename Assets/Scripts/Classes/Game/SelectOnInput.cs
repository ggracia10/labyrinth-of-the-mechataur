﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {

    public EventSystem eventSystem;
    public GameObject selectedButton;
    public bool buttonIsSelected;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Vertical") != 0 && !buttonIsSelected)
        {
            eventSystem.SetSelectedGameObject(selectedButton);
            buttonIsSelected = true;
        }

    }
    private void OnDisable()
    {
        buttonIsSelected = false;
    }
}
