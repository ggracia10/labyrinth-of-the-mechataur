﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Entity : MonoBehaviour {
    
    public struct Stats
    {
        public string name;
        public int hpMax, hp, atk, def, skin, ammoCount;
        public float maxSpeed;
        public Weapon.WeaponEnum currentWeapon;
        public Color color;

        //!  Initialization of the struct
        public Stats(string newName, int newHpMax, int newAtk, int newDef, float newMaxSpeed,
            Weapon.WeaponEnum newWeapon, int newSkin, Color newColor)
        {
            this.name = newName;
            this.hpMax = newHpMax;
            this.hp = this.hpMax;
            this.atk = newAtk;
            this.def = newDef;
            this.maxSpeed = newMaxSpeed;
            this.currentWeapon = newWeapon;
            this.skin = newSkin;
            this.color = newColor;
            this.ammoCount = 0;
            return;
        }
    }
    
    protected Transform trans;
    protected Rigidbody2D rb2d;
    public enum Direction {None, Up, Down, Left, Right};
   
    protected Direction currentDirection;
    protected Direction lastDirection;
    
    protected Animator anim, animSkin, animWeapon;
    protected bool changeAnim, attacking, stuned, moving;

    protected Stats myStats;
    protected float currentSpeedX, currentSpeedY, currentAngle;
    protected const float colSpeedTrans = 0.8F;
    protected string filePath;
    protected string jsonString;
    protected SpriteRenderer mSpriteRender;
    protected float coolDown, currentCoolDown;
    public float timeStuned, timeStunedMax;
    protected float tempInvencible, tempInvencibleMax;
    protected bool inATrap;
    protected float timeRocketImpact, timeMaxRocketImpact;


    //! Function of movement
    protected void Movement(float speedX, float speedY, float newAngle, bool newMoving)
    {
        //! We safe the position on a temporal variable
        Vector3 tempPos = gameObject.transform.position;

        if (!newMoving)
        {
            speedX = 0; speedY = 0;
        }
        //Debug.Log(currentSpeedX + "," + currentSpeedY);
        //! Set direction
        

        //! First we move the player and later we will check if is collisioning
        //! If it's collisioning it will set the position that it's saved on the temporal variable
        if(newAngle != 0 && newAngle != 90 && newAngle != 270 && newAngle != 360)
        {
            speedX *= Mathf.Cos(newAngle);
            speedY *= Mathf.Sin(newAngle);
        }
        
        trans.position += new Vector3(speedX, speedY, trans.position.z) * Time.deltaTime;

            //! If it's collisioning on the two directions (up/down and left/right) we will set all the position (all the components)
            if ((CheckCollision(this.gameObject.transform.GetChild(4).position, Vector2.right)||
            CheckCollision(this.gameObject.transform.GetChild(3).position, Vector2.left)) &&
            (CheckCollision(this.gameObject.transform.GetChild(1).position, Vector2.up) ||
            CheckCollision(this.gameObject.transform.GetChild(2).position, Vector2.down)))
        {
            //speedX = -colSpeedTrans;
            trans.position = tempPos;
            speedY = 0;
            speedX = 0;
            newAngle = 0;
        }
        else
        {
            //! If it's collisioning only on the right/left set as the position of X, the temporal pos X
            if (CheckCollision(this.gameObject.transform.GetChild(4).position, Vector2.right) &&  speedX > 0||
                CheckCollision(this.gameObject.transform.GetChild(3).position, Vector2.left) && speedX < 0)
            {
                //speedX = -colSpeedTrans;
                speedY /= Mathf.Cos(newAngle);
                speedX = 0;

            }
            //! If it's collisioning only on the right/left set as the position of Y, the temporal pos Y
            if (CheckCollision(this.gameObject.transform.GetChild(1).position, Vector2.up) && speedY > 0 ||
                CheckCollision(this.gameObject.transform.GetChild(2).position, Vector2.down) && speedY < 0)
            {
                speedX /= Mathf.Cos(newAngle);
                speedY = 0;
                
            }
            trans.position = tempPos + new Vector3(speedX, speedY, 0) * Time.deltaTime;
        }
        // Esto es para que solo cambie de direccion
        
        if (speedY == 0)
        {
            if (speedX > 0.6F * myStats.maxSpeed)
            {
                currentDirection = Direction.Right;
            }
            else if (speedX < -0.6F * myStats.maxSpeed)
            {
                currentDirection = Direction.Left;
            }
        }
        else if (speedX == 0)
        {
            if (speedY > 0.6F * myStats.maxSpeed)
            {
                currentDirection = Direction.Up;

            }
            else if (speedY < -0.6F * myStats.maxSpeed)
            {
                currentDirection = Direction.Down;
            }
        }
        

        //! Compare the last direction with the current direction to control the animation change
        /*if (lastDirection != currentDirection)
        {
            changeAnim = true;
        }*/
        if (lastDirection == Direction.Down && currentDirection == Direction.Up ||
            lastDirection == Direction.Up && currentDirection == Direction.Down ||
            lastDirection == Direction.Left && currentDirection == Direction.Right ||
            lastDirection == Direction.Right && currentDirection == Direction.Left)
        {
            changeAnim = true;
        }
        
        
        return;
    }

    //! Reset variables to 0
    protected void ResetVariables()
    {
        if(myStats.currentWeapon == Weapon.WeaponEnum.None)
        {
            myStats.ammoCount = 2;
        }
        changeAnim = false;
        currentAngle = 0;
        currentSpeedX = 0;
        currentSpeedY = 0;
        moving = false;
        return;
    }

    //! Here we will check collisions of the object
    protected bool CheckCollision(Vector2 newVect, Vector2 newDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(newVect, newDir, 0.001F);
        return hit;
    }

    //! The function that will be generic of the attack of every Entity.
    virtual protected void Attack(Stats tempStats)
    {

    }

    //! The function that will be used when an Entity is beeing attacked by an other one
    //! We will call this function when two Entities are collisioning and one of them is attacking (melee weapon) 
    //! or when a shot is collisioning whith an Entity (distance weapon)
    virtual public void Stunned (Bullet.BulletEnum typeBullet, Direction newDir, float newDamage)
    {
        //! Set the function in the Player and in the Boss
    }

    //! the function that will be used when Entity is iside or is hit by a trap
    //! It interacts the same as Stunned function, but with traps classes.
    virtual protected void TrapHit(Trap.TrapsEnum trapType, bool newStunned, float newDamage)
    {
        //! Set the function in the Player and in the Boss
    }




    //! This function change the variables on the animator controller to change the animation of the sprite.
    protected void ChangeAnimation(float newXSpeed, float newYSpeed, bool newAttacking, 
        int newCurrWeapon, bool newStuned, bool newChangeAnim, int newDirection, Stats newStats)
    {
        anim.SetFloat("xSpeed", newXSpeed);
        anim.SetFloat("ySpeed", newYSpeed);
        anim.SetInteger("CurrentWeapon", newCurrWeapon);
        anim.SetBool("changeAnim", newChangeAnim);
        anim.SetInteger("Direction", newDirection);

        if (newStats.skin != 0 && gameObject.name != "Mechataur")
        {
            animSkin.SetFloat("xSpeed", newXSpeed);
            animSkin.SetFloat("ySpeed", newYSpeed);
            animSkin.SetInteger("CurrentWeapon", newCurrWeapon);
            animSkin.SetBool("changeAnim", newChangeAnim);
            animSkin.SetInteger("Direction", newDirection);
        }
        
        string typeOfAnimation;
        string weapon;

        //! This switch changes the animator controller of the players deppending of the weapon
        switch (newCurrWeapon)
        {
            default:
                weapon = "Gun";
                break;
            case 0:
                weapon = "None";
                break;
            /*case 1:
                weapon = "Sword";
                break;*/
        }

        if (weapon != "None")
        {
            //This will be the final function, but until we have all the animations done
            //We will put the controller directly

            /*animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)
                    Resources.Load("Animators/Weapons/" + newStats.curretWeapon);*/
            //***********************************************************************
            if (newStats.currentWeapon == Weapon.WeaponEnum.Pistol)
            {
                animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Weapons/Pistol");
            }
            else if (newStats.currentWeapon == Weapon.WeaponEnum.Shotgun)
            {
                animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Weapons/Shotgun");
            }
            else if (newStats.currentWeapon == Weapon.WeaponEnum.Uzi)
            {
                animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Weapons/Uzi");
            }
            else if (newStats.currentWeapon == Weapon.WeaponEnum.RocketLauncher ||
                newStats.currentWeapon == Weapon.WeaponEnum.Sword)
            {
                animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Weapons/RocketLauncher");
            }
            //***********************************************************************
        }
        else
        {
            if (newStats.currentWeapon == Weapon.WeaponEnum.None && gameObject.name != "Mechataur")
            {
                animWeapon.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animators/Weapons/None");
            }
        }
        //if current weapon is not NONE we will change the animator variables
        if (newStats.currentWeapon != 0)
        {
            animWeapon.SetFloat("xSpeed", newXSpeed);
            animWeapon.SetFloat("ySpeed", newYSpeed);
            animWeapon.SetInteger("CurrentWeapon", newCurrWeapon);
            animWeapon.SetBool("changeAnim", newChangeAnim);
            animWeapon.SetInteger("Direction", newDirection);
        }

        //! Set the type of the animation (attack, stuned or run)
        /*if (newAttacking)
        {
            typeOfAnimation = "Attack";
        }
        /*else if (newStuned)
        {
            typeOfAnimation = "Stunned";
        }
        else
        {
            typeOfAnimation = "Run";
        }*/
        typeOfAnimation = "Run";
        //! Change the animator controller of the Entity
        if (gameObject.name != "Mechataur")
        {
            anim.runtimeAnimatorController = (RuntimeAnimatorController)
                Resources.Load("Animators/Model/" + typeOfAnimation + weapon + "Animator");
        }
        //! Change the animator controller of the skin if it's equiped one
        if (newStats.skin != 0 && gameObject.name != "Mechataur")
        {
            animSkin.runtimeAnimatorController = (RuntimeAnimatorController)
            Resources.Load("Animators/Model/Skin" + newStats.skin); 
        }
        GenericFunctions.setRenderOrder(gameObject);
        GenericFunctions.setRenderOrder(gameObject.transform.GetChild(0).gameObject);
        GenericFunctions.setRenderOrder(gameObject.transform.GetChild(6).gameObject);
        return;
    }

    //! Function when the entity dies
    virtual public void Die()
    {
        //code
        return;
    }
    void PickWeapon()
    {
        //code
        return;
    }
    void ThrowWeapon()
    {
        //code
        return;
    }
}

public class infoEntity
{
    public string name;
    public int maxHP;
    public int attack;
    public int defense;
    public int maxSpeed;
}
